import React, { useState } from 'react';
import MaterialTable from 'material-table';
import { BASE_URL } from '../../common/Constant';

export default function EditableTableRatios(props) {
  const rows = props.rows;
  const columns = [
    { title: 'name', field: 'name', editable: 'never' },
    { title: 'minRatio%', field: 'minRatio', type: 'numeric' },
    { title: 'maxRatio%', field: 'maxRatio', type: 'numeric' },
  ];
  // eslint-disable-next-line
  const [updatedCountry, setUpdatedCountry] = useState({});
  const [data, setData] = useState(rows)

  return (
    <MaterialTable
      title="Country Ratios"
      columns={columns}
      data={data}
      cellEditable={{
        cellStyle: {},
        onCellEditApproved: (newValue, oldValue, rowData, columnDef) => {
          return new Promise((resolve, reject) => {
            console.log('newValue: ' + newValue);
            setTimeout(resolve, 4000);
          });
        }
      }}
      editable={{
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                console.log(oldData);
                console.log(newData.name);
                setUpdatedCountry(newData);
                fetch(`${BASE_URL}/countries/`, {
                  method: 'PUT',
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
                  },
                  body: JSON.stringify({
                    name: newData.name,
                    minRatio: newData.minRatio / 100,
                    maxRatio: newData.maxRatio / 100,
                  }),
                })

                setData((prevState) => {
                  console.log(prevState)
                  const data = [...prevState];
                  data[data.indexOf(oldData)] = newData;
                  return [...data];
                });
              }
            }, 600);
          }),
      }}
    />
  )
}
