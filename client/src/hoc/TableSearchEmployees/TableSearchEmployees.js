import React, { useState } from 'react';
import MaterialTable from 'material-table';
import { BASE_URL } from '../../common/Constant';

export default function TableSearchEmployees(props) {
  const rows = props.rows;
  const columns = [
    { title: 'Id', field: 'id', editable: 'never' },
    { title: 'Fullname', field: 'fullName', editable: 'never' },
    { title: 'Country', field: 'country', editable: 'never' },
    { title: 'Project', field: 'project', editable: 'never' },
    { title: 'Current Week Location', field: 'currentWeekLocation', editable: 'never' },
    { title: 'Next Week Location', field: 'nextWeekLocation', editable: 'never' },
    { title: 'Role', field: 'role' },
  ];

  // eslint-disable-next-line
  const [updatedRole, setUpdatedRole] = useState('');

  const [data, setData] = useState(rows);

  return (
    <MaterialTable
      title="All Employees"
      columns={columns}
      data={data}
      cellEditable={{
        cellStyle: {},
        onCellEditApproved: (newValue) => {
          return new Promise((resolve) => {
            console.log('newValue: ' + newValue);
            setTimeout(resolve, 4000);
          });
        },
      }}
      editable={{
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();

              if (newData.role !== 'Admin' && newData.role !== 'Basic') {
                alert('Invalid Role, choose only Basic or Admin');
              }

              if (oldData) {
                setUpdatedRole(newData);
                console.log(oldData);
                fetch(`${BASE_URL}/admin/users/${oldData.id}/changeRole/`, {
                  method: 'PUT',
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
                  },
                  body: JSON.stringify({
                    role: newData.role === 'Admin' ? 2 : 1,
                  }),
                })

                setData((prevState) => {
                  console.log(prevState)
                  const data = [...prevState];
                  data[data.indexOf(oldData)] = newData;
                  return [...data];
                });
              }
            }, 600);
          }),
      }}
    />
  );
}
