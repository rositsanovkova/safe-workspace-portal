import React from 'react';
import './SimpleButton.css';

const SimpleButton = (props) => {
  const onClick = props.onClick;

  return (
    <button className="myButton" onClick={onClick}>
      {props.children}
    </button>
  );
};

export default SimpleButton;
