import React, { useState } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import AuthContext, { extractUser, getToken } from './providers/AuthContext';
import GuardedRoute from './providers/GuardedRoutes';
import Navigation from './components/Navigation/Navigation';
import SignIn from './components/SignIn/SignIn';
import Home from './components/Home/Home';
import CreateProject from './components/Project/CreateProject/CreateProject';
import AllCompanies from './components/Companies/AllCompanies/AllCompanies';
import IndividualProject from './components/Project/IndividualProject/IndividualProject';
import AllProjects from './components/Project/AllProjects/AllProject';
import IndividualCompany from './components/Companies/IndividualCompany/IndividualCompany';
import FloorPlanning from './components/Floor-planning/FloorPlanning';
import CreateWorkStation from './components/WorkStation/CreateWorkStation';
import CreateFloorPlanning from './components/Floor-planning/CreateFloorPlanning/CreateFloorPlanning';
import Profile from './components/Profile/Profile';
import EditProfile from './components/Profile/EditProfile/EditProfile';
import ContactUs from './components/ContactUs/ContactUs';
import '../src/styles.css';
import AllVacations from './components/Vacation/AllVacations/AllVacations';
import Sidebar from './components/Navigation/Sidebar';
import AllEmployees from './components/Employees/AllEmployees/AllEmployees';
import AllCountries from './components/AllCountries/AllCountries';

function App() {
  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    loggedUser: extractUser(getToken()),
  });

  return (
    <BrowserRouter>
      <AuthContext.Provider
        value={{ ...authValue, setLoginState: setAuthValue }}
      >
        <div className="Navigation">
          <Navigation />
        </div>
        <div className="app-flex-container">
          {authValue.isLoggedIn && authValue.loggedUser.role === 2 ? (
            <div className="sidebar">
              <Sidebar className="sidebar-menu" />
            </div>
          ) : null}

          <div className="main-content">
            <Switch>
              <Redirect path="/" exact to="/home" />
              <Route path="/home" exact component={Home} />
              <Route path="/login" component={SignIn} />
              <Route path="/register" component={SignIn} />
              <GuardedRoute
                path="/companies"
                exact
                component={AllCompanies}
                auth={authValue.isLoggedIn}
              />
              <GuardedRoute
                path="/companies/:id"
                exact
                component={IndividualCompany}
                auth={authValue.isLoggedIn}
              />
              {/* <GuardedRoute path="/employees/available" exact component={CreateProject} auth={authValue.isLoggedIn}/> */}
              <GuardedRoute
                path="/:id/project/create"
                exact
                component={CreateProject}
                auth={authValue.isLoggedIn}
              />
              <GuardedRoute
                path="/project"
                exact
                component={AllProjects}
                auth={authValue.isLoggedIn}
              />
              <GuardedRoute
                path="/project/:id"
                exact
                component={IndividualProject}
                auth={authValue.isLoggedIn}
              />

              <GuardedRoute
                path="/:id/workStation/create"
                exact
                component={CreateWorkStation}
                auth={authValue.isLoggedIn}
              />
              <GuardedRoute
                path="/floor-planning/create"
                exact
                component={CreateFloorPlanning}
                auth={authValue.isLoggedIn}
              />
              <GuardedRoute
                path="/:id/floor-planning"
                exact
                component={FloorPlanning}
                auth={authValue.isLoggedIn}
              />
              <GuardedRoute
                path="/profile"
                exact
                component={Profile}
                auth={authValue.isLoggedIn}
              />
              <GuardedRoute
                path="/users/:id"
                exact
                component={EditProfile}
                auth={authValue.isLoggedIn}
              />
              <GuardedRoute
                path="/email"
                exact
                component={ContactUs}
                auth={authValue.isLoggedIn}
              />
              <GuardedRoute
                path="/employees"
                exact
                component={AllEmployees}
                auth={authValue.isLoggedIn}
              />

              <GuardedRoute
                path="/vacations"
                exact
                component={AllVacations}
                auth={authValue.isLoggedIn}
              />
              <GuardedRoute
                path="/countries"
                exact
                component={AllCountries}
                auth={authValue.isLoggedIn}
              />
            </Switch>
          </div>
        </div>
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;
