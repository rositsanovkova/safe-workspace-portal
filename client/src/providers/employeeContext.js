
import React, { createContext, useState, useContext } from 'react';

export const EmployeeContext = createContext();
export const UpdateEmployeeContext = createContext();

export function useEmployeeId() {
  return useContext(EmployeeContext);
}

export function useEmployeeIdUpdate() {
  return useContext(UpdateEmployeeContext);
}

export function EmployeeProvider({ children }) {
  const [employeeID, setEmployeeID] = useState(0);

  function toggleUpdate() {
    setEmployeeID(employeeID+1)
  }

  return (
    <EmployeeContext.Provider value={employeeID}>
      <UpdateEmployeeContext.Provider value={toggleUpdate}>
        {children}
      </UpdateEmployeeContext.Provider>
    </EmployeeContext.Provider>
  )
}

