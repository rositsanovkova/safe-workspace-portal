import { createContext } from 'react';

const CounterContext = createContext({
  count: 0,
  setCountState: () => {},
});

export default CounterContext;
