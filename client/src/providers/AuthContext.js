/* eslint-disable no-else-return */
import { createContext } from 'react';
import jwtDecode from 'jwt-decode';

const AuthContext = createContext({
  isLoggedIn: false, // true or false - is the user logged in
  loggedUser: null, // null or the logged user's payload
  setLoginState: () => {},
});

export const getToken = () => {
  if (localStorage.getItem('token')) {
    return localStorage.getItem('token');
  } else {
    return '';
  }
};

export const extractUser = (token) => {
  try {
    return jwtDecode(token);
  } catch (e) {
    localStorage.removeItem('token');
    return null;
  }
};

export default AuthContext;
