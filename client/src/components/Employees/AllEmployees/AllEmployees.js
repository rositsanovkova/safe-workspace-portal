import React, { useState, useEffect } from 'react';
import { BASE_URL } from '../../../common/Constant';
import { notify } from '../../../common/Notification';
import TableSearchEmployees from '../../../hoc/TableSearchEmployees/TableSearchEmployees';

const AllCountries = (props) => {
  const [employees, setEmployees] = useState([]);
  // eslint-disable-next-line
  const [loading, setLoading] = useState(false);
  // eslint-disable-next-line
  const [error, setError] = useState(null);
  // eslint-disable-next-line
  const [length, setLength] = useState(0);

  let data = [];

  useEffect(() => {
    fetch(`${BASE_URL}/employees`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((employees) => {
          if (employees.message) {
            setEmployees('');
            notify(`${employees.message}`);
          } else {
            employees.forEach((employee) => {
              data.push({
                id: employee.user.id,
                fullName: employee.user.fullName,
                country: employee.company.country,
                project: employee.project ?
                 employee.project.title :
                  'Not Working on any project',
                currentWeekLocation: employee.workSchedule ?
                    employee.workSchedule.currentWeekLocation === 1 ?
                     'Home' :
                      employee.workSchedule.currentWeekLocation === 2 ?
                       'Office' :
                        employee.workSchedule.currentWeekLocation === 3 ?
                         'Vacation' :
                          null :
                           'No working Location',

                nextWeekLocation: employee.workSchedule ?
                 employee.workSchedule.nextWeekLocation === 1 ?
                  'Home' :
                   employee.workSchedule.nextWeekLocation === 2 ?
                    'Office' :
                     employee.workSchedule.nextWeekLocation === 3 ?
                      'Vacation' :
                       null :
                        'No working Location',
                role: employee.user.role ?
                 employee.user.role === 1 ?
                  'Basic' :
                   'Admin' :
                    null,
              });
            });

            setLength(employees.length);
          }
        })
        .catch((error) => setError(error.message));
  }, [employees, data]);

  return (
    <div>
      {loading ? <h1>Loading...</h1> : null}
      <>
        <TableSearchEmployees rows={data} history={props.history} />
      </>
    </div>
  );
};

export default AllCountries;
