import React, { useContext } from 'react';
import emailjs from 'emailjs-com';
import AuthContext from '../../providers/AuthContext';

const ContactUs = () => {
  // eslint-disable-next-line
  const { loggedUser } = useContext(AuthContext);
  function sendEmail(e) {
    e.preventDefault();
    emailjs.sendForm('service_5uut0wf', 'template_ipn1v2c', e.target, 'user_1XJGgrFHuOAbxKhqCBPMu')
        .then((result) => {
          console.log(result.text);
        }, (error) => {
          console.log(error.text);
        });
    e.target.reset();
  }

  return (
    <form className="contact-form" onSubmit={sendEmail}>
      <input type="hidden" name="contact_number" />
      <p><label>Subject:    </label>
        <input type="subject" name="subject" /></p>
      <p><label>Name</label>
        <input type="text" name="user_name" /></p>
      <p><label>Email</label>
        <input type="email" name="user_email" /></p>
      <p><label>Message</label>
        <textarea name="message" /></p>
      <input type="submit" value="Send" />
    </form>
  );
}

export default ContactUs;
