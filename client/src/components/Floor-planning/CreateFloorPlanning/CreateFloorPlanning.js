import React, { useState, useContext } from 'react';
import { Form } from 'react-bootstrap';
import { BASE_URL } from '../../../common/Constant'
import AuthContext from '../../../providers/AuthContext';
import SimpleButton from '../../../hoc/SimpleButton/SimpleButton';

const CreateFloorPlanning = (props) => {
  const history = props.history;
  // eslint-disable-next-line
  const { isLoggedIn, loggedUser, setLoginState } = useContext(AuthContext);
  const [companies, setCompanies] = useState('');
  // eslint-disable-next-line
  const [chosenCompany, setChosenCompany] = useState('');
  const [floorPlanning, setFloorPlanning] = useState({
    numberOfDesks: {
      value: 0,
      touched: false,
      valid: true,
    },
    workStation: {
      value: [],
      touched: false,
      valid: true,
    },
    workStationColumns: {
      value: 0,
      touched: false,
      valid: true,
    },
    company: {
      value: '',
      touched: false,
      valid: true,
    }
  })


  const updateFloorPlanning = (prop, value) => {
    setFloorPlanning({
      ...floorPlanning,
      [prop]: {
        value,
        touched: true,
        valid: floorPlanningValidators[prop].reduce(
            (isValid, validatorFn) =>
              isValid && typeof validatorFn(value) !== 'string',
            true
        ),
      },
    });
  }

  const floorPlanningValidators = {
    workStationColumns: [
      (value) => value >= 1 || `Distance between desks should be at least 1.5 meters!`,
    ],
    numberOfDesks: [
      (value) => value >= 1 || `There should be at least one desk!`,
    ],
    company: [
      (value) => value?.length >= 1 || `Company should be selected!`,
    ],
  };

  const getValidationErrors = (prop) => {
    return floorPlanningValidators[prop]
        .map((validatorFn) => validatorFn(floorPlanning[prop].value)) // [string, true, true, string]
        .filter((value) => typeof value === 'string');
  };

  const renderValidationError = (prop) =>
    floorPlanning[prop].touched && !floorPlanning[prop].valid ?
     getValidationErrors(prop).map((error, index) => (
       <p className="error" key={index}>
         {error}
       </p>
     )) :
      null;

  const getClassNames = (prop) => {
    let classes = '';
    if (floorPlanning[prop].touched) {
      classes += 'touched';
    }
    if (floorPlanning[prop].valid === true) {
      classes += 'valid';
    }
    if (floorPlanning[prop].valid === false) {
      classes += 'invalid';
    }
    return classes;
  };


  const create = () => {
    if (!floorPlanning.workStationColumns.valid || floorPlanning.workStationColumns.value === 0) {
      return alert('You should place at least 1 column');
    }
    if (!floorPlanning.company.valid || floorPlanning.company.value === '') {
      return alert('Invalid company!');
    }
    if (!floorPlanning.numberOfDesks.valid || floorPlanning.numberOfDesks.value === 0) {
      return alert('Invalid number of desks!');
    }

    fetch(`${BASE_URL}/floor-plannings`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },

      body: JSON.stringify({
        // ...user,
        numberOfDesks: floorPlanning.numberOfDesks.value,
        workStationColumns: floorPlanning.workStationColumns.value,
        company: floorPlanning.company.value,
      }),
    })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return alert(result.message);
          }
          history.push(``);
          history.push(`/companies`);
        })
        .catch(alert);
  };


  const getCompanies = () => {
    fetch(`${BASE_URL}/floor-plannings/companies/available`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return alert(result.message);
          }
          const allCountries = result.map((r) => r.country);
          setCompanies(allCountries);
        })
        .catch(alert);
  };
  console.log(history)
  return (

    <div className="create-floor-planning">
      <Form className="form-content-floor-planning">
        <Form.Group controlId="form-number-desks-floor-planning">
          <Form.Label>
            <h4>Number of Desks: </h4>
          </Form.Label>

          <Form.Control
            type="number"
            placeholder="&#xF007; Number of Desks"
            className={getClassNames('numberOfDesks')}
            value={floorPlanning.numberOfDesks.value}
            onChange={(e) => updateFloorPlanning('numberOfDesks', e.target.value)}
          />
          <Form.Text className="text-muted">
            {renderValidationError('numberOfDesks')}
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="form-workStationColumns-floor-planning">
          <Form.Label>
            <h5>WorkStation Columns: </h5>
          </Form.Label>
          <Form.Control
            type="number"
            placeholder="&#xF007; workStation Columns"
            className={getClassNames('workStationColumns')}
            value={floorPlanning.workStationColumns.value}
            onChange={(e) => updateFloorPlanning('workStationColumns', e.target.value)}
          />
          <Form.Text className="text-muted">
            {renderValidationError('workStationColumns')}
          </Form.Text>
        </Form.Group>
        <Form.Group style={{ width: '500px' }} controlId="form-workStation-floor-planning.ControlSelect2">
          <Form.Control as="select" multiple onClick={()=>getCompanies()} >
            <option>Company select:</option>
            {companies.length>0 ? (companies.map((r) => (<option key={r} onClick={(e) => updateFloorPlanning('company', e.target.value)}>{r}</option>))) : null}
          </Form.Control>
          <Form.Text className="text-muted">
            {renderValidationError('company')}
          </Form.Text>
        </Form.Group>
      </Form>
      <SimpleButton onClick={create}>Create</SimpleButton>
      <SimpleButton onClick={() => history.goBack()}>Back</SimpleButton>
    </div>
  );
};

export default CreateFloorPlanning;
