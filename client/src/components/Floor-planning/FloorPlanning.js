import React, { useState, useEffect, useContext } from 'react';
import AuthContext from '../../providers/AuthContext';
import WorkStation from '../WorkStation/WorkStation';
import { BASE_URL } from '../../common/Constant';
import { notify } from '../../common/Notification';
import '../Floor-planning/FloorPlanning.css';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';
import NextWeekWorkStation from '../WorkStation/NextWeekWorkStation';

const FloorPlanning = (props) => {
  const match = props.match;
  // eslint-disable-next-line
  const [desks, setDesks] = useState(props.desks);
  const { loggedUser } = useContext(AuthContext);

  const [floorPlanning, setFloorPlanning] = useState({});
  const [desksToPlace, setDesksToPlace] = useState(floorPlanning.desks);
  const [week, setWeek] = useState('current');
  const [currentEmployees, setCurrentEmployees] = useState([]);
  // eslint-disable-next-line
  const [loading, setLoading] = useState(false);
  // eslint-disable-next-line
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/floor-plannings/${match.params.id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((response) => response.json())
        .then((floorPlanning) => {
          if (floorPlanning.message) {
            notify(`${floorPlanning.message}`);
          }
          setFloorPlanning(floorPlanning);
          const remainingDesks =
          floorPlanning.numberOfDesks -
          floorPlanning.workStation.reduce(
              (acc, workstation) => (acc += workstation.desks.length),
              0
          );
          setDesksToPlace(remainingDesks);
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  }, [match.params.id, desksToPlace]);

  const arrowFields = [];
  for (let i = 1; i <= floorPlanning.workStationColumns; i++) {
    arrowFields.push(i);
  }
  console.log(floorPlanning);
  console.log(floorPlanning.workStation);

  return (
    <>
      {desksToPlace === 0 || loggedUser.role === 1 ? null : (
        <SimpleButton
          onClick={() =>
            props.history.push(
                `/${floorPlanning.company.id}/workStation/create`
            )
          }
        >
          CreateWorkStation
        </SimpleButton>
      )}

      <SimpleButton onClick={() => setWeek('next')}>
        currentWeekSitting
      </SimpleButton>
      <SimpleButton onClick={() => setWeek('current')}>
        nextWeekSitting
      </SimpleButton>
      <p>Number of Total Desks: {floorPlanning.numberOfDesks}</p>
      <p>Number of Desks to be placed: {desksToPlace}</p>
      <div
        className="wrapper"
        style={{ '--gridTemplateColumns': floorPlanning.workStationColumns }}
      >
        {floorPlanning.workStation ?
         floorPlanning.workStation.map((workstation, index) => {
           console.log(workstation.id);
           return (
             <div
               id={workstation.id}
               key={`id-${workstation.id}`}
               className="elements"
             >
               {week === 'current' ? (
                    <WorkStation
                      key={`workstation-${workstation.id}`}
                      workstation={workstation}
                      floorPlanning={floorPlanning}
                      currentEmployees={currentEmployees}
                    />
                  ) : (
                    <NextWeekWorkStation
                      key={`workstation-${workstation.id}`}
                      workstation={workstation}
                      floorPlanning={floorPlanning}
                      currentEmployees={currentEmployees}
                    />
                  )}

               {index !== floorPlanning.workStation.length - 1 ? (
                    (index + 1) % floorPlanning.workStationColumns !== 0 ? (
                      <span id={index} className="row-distance">
                        <i className="fas fa-angle-double-right"></i>
                        <span>3m</span>{' '}
                        <i className="fas fa-angle-double-right"></i>
                      </span>
                    ) : (
                      <i className="fas fa-genderless"></i>
                    )
                  ) : (
                    <i className="fas fa-genderless"></i>
                  )}
               {index <=
                    floorPlanning.workStation.length -
                      1 -
                      (floorPlanning.workStation.length %
                        floorPlanning.workStationColumns) &&
                  floorPlanning.workStation.length !==
                    floorPlanning.workStationColumns ? (
                    floorPlanning.workStation.length %
                      floorPlanning.workStationColumns ===
                    0 ? (
                      index <=
                      floorPlanning.workStation.length -
                        1 -
                        floorPlanning.workStationColumns ? (
                        <div
                          id={index}
                          key={`id-${workstation.id}`}
                          className="col-distance"
                        >
                          <i className="fas fa-angle-double-down"></i>
                          <span>2m</span>{' '}
                          <i className="fas fa-angle-double-down"></i>
                        </div>
                      ) : null
                    ) : null
                  ) : null}
             </div>
           );
         }) :
           null}
      </div>
    </>
  );
};
export default FloorPlanning;
