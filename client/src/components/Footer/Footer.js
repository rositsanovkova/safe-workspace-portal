import React from 'react';
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from 'mdbreact';
import Popup from 'reactjs-popup';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton'
import ContactUs from '../ContactUs/ContactUs';

const Footer = () => {
  return (
    <MDBFooter className="footer-main">
      <MDBContainer fluid className="text-center">
        <MDBRow>
          <MDBCol style={{ width: '150px' }}>
            <h5 className="title">Created by:</h5>
            <h5 style={{ color: 'black' }}>Silviya Naumova</h5>
            <h5 style={{ color: 'black' }}>Rositsa Novkova</h5>
            <Popup trigger={<SimpleButton>Contact us</SimpleButton>} position="center" modal nested>
              {(close) => (
                <>
                  <button className="close" onClick={close}>
                    &times;
                  </button>
                  <div className="header"> Contact Us </div>
                  <div className="content">
                    <span>
                      <ContactUs></ContactUs>
                    </span>
                  </div>
                </>
              )}
            </Popup>
          </MDBCol>
          <MDBCol style={{ width: '150px' }}>
            <h5 className="title">Technologies:</h5>
            <img
              style={{ width: '100px' }}
              src="https://dwglogo.com/wp-content/uploads/2017/09/1460px-React_logo.png"
              className="img-fluid"
              alt=""
            />
            <img
              style={{ width: '70px' }}
              src="https://docs.nestjs.com/assets/logo-small.svg"
              className="img-fluid"
              alt=""
            />
          </MDBCol>
          <MDBCol style={{ width: '300px' }}>
            <h5 className="title">Supported by:</h5>
            <img
              style={{ width: '150px' }}
              src="https://eskills.tto-bait.bg/wp-content/uploads/2017/03/Primary@2x.png"
              className="img-fluid"
              alt=""
            />
            <img
              style={{ width: '150px' }}
              src="https://balkantech.eu/wp-content/uploads/2020/07/bright_consulting_logo.png"
              className="img-fluid"
              alt=""
            />
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </MDBFooter>
  );
};

export default Footer;
