import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { BASE_URL } from '../../../common/Constant';
import { Form } from 'react-bootstrap';
import SimpleButton from '../../../hoc/SimpleButton/SimpleButton';
import { notify } from '../../../common/Notification';

const IndividualProject = (props) => {
  const match = props.match;
  const history = props.history;
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [project, setProject] = useState({
    title: '',
    company: '',
    employees: [],
  });
  const [countries, setCountries] = useState([]);
  const [country, setCountry] = useState([]);
  const [currentCountry, setCurrentCountry] = useState('');
  const [availableEmployees, setAvailableEmployees] = useState([]);
  const [assignEmployees, setAssignEmployees] = useState([]);
  const [currentEmployees, setCurrentEmployees] = useState('');
  const [deletedEmployee, setDeletedEmployee] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/project/${match.params.id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((response) => response.json())
        .then((project) => {
          if (project.message) {
            notify(`${project.message}`);
          }
          const employees = project.employees;
          setProject(project);
          setCurrentCountry('');
          setDeletedEmployee(false);
          setCurrentEmployees('');
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  }, [match.params.id, currentCountry, currentEmployees, deletedEmployee]);

  const updateCountry = (country) => {
    setCountry(country);
  };

  const updateEmployees = (employee) => {
    if (!assignEmployees.includes(employee)) {
      setAssignEmployees([...assignEmployees, employee]);
    }
  };

  const updateCompany = () => {
    fetch(`${BASE_URL}/project/${project.id}/country`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({ country: country }),
    })
        .then((response) => response.json())
        .then((result) => {
          if (result.error) {
            throw new Error(result.message);
          }
          setCurrentCountry(result.country);
        })
        .catch((error) => setError(error.message));
  };

  const getCountry = () => {
    fetch(`${BASE_URL}/companies`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return alert(result.message);
          }
          const allCountries = result.filter(
              (r) => r.country !== project.company.country
          );
          setCountries(allCountries);
        })
        .catch(alert);
  };

  const getEmployees = () => {
    fetch(`${BASE_URL}/employees/available/${project.company.id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return alert(result.message);
          }
          if (result.length === 0) {
            return notify('No more employees are available');
          }
          const allEmployees = result.map((r) => r.user.fullName);
          setAvailableEmployees(allEmployees);
        })
        .catch(alert);
  };

  const reassignEmployees = () => {
    setLoading(true);
    assignEmployees.map((employee) => {
      fetch(`${BASE_URL}/project/${project.id}/employees`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
        },
        body: JSON.stringify({ fullName: employee }),
      })
          .then((response) => response.json())
          .then((result) => {
            if (result.error) {
              throw new Error(result.message);
            }
            setCurrentEmployees(result.employees);
            setAvailableEmployees([]);
            setAssignEmployees([]);
          })
          .catch((error) => setError(error.message))
          .finally(() => setLoading(false));
    });
  };

  const deleteEmployee = (employeeId) => {
    setLoading(true);
    fetch(`${BASE_URL}/project/${match.params.id}/delete/${employeeId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((response) => response.json())
        .then((result) => {
          if (result.error) {
            throw new Error(result.message);
          }
          setProject({
            employees: result.employees,
            title: project.title,
            company: project.company,
          });
          setDeletedEmployee(true);
          setCurrentEmployees(result.employees);
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  };

  return (
    <div>
      {loading ? <h1>Loading...</h1> : null}

      <>
        <p>Title: {project.title}</p>
        <p>Company: {project.company.country}</p>

        <Form.Group controlId="form-individual-project">
          <Form.Control
            style={{ width: '270px' }}
            as="select"
            onClick={() => getCountry()}
            onChange={(e) => updateCountry(e.target.value)}
          >
            <option>Country select:</option>
            {countries.length > 0 ?
             countries.map((r) => <option key={r.id}>{r.country}</option>) :
              null}
          </Form.Control>
        </Form.Group>
        <SimpleButton onClick={() => updateCompany()}>Update</SimpleButton>
        <br></br>
        <p>Employees: </p>
        {project.employees && project.employees.length > 0 ? (
          project.employees.map((employee) => (
            <div key={employee.id} employee={employee}>
              <span>
                {employee.user.fullName}
                <SimpleButton onClick={() => deleteEmployee(employee.id)}>
                  Delete
                </SimpleButton>
              </span>
            </div>
          ))
        ) : (
          <p>No employees</p>
        )}

        <Form.Group
          style={{ width: '270px' }}
          controlId="form-individual-project-employees.ControlSelect2"
        >
          <Form.Label>
            <p>Employee Select:</p>
          </Form.Label>
          <Form.Control
            className="employees-individual-project"
            as="select"
            multiple
            onClick={() => getEmployees()}
          >
            {availableEmployees.length > 0 ?
             availableEmployees.map((r) => (
               <option
                 key={r}
                 onClick={(e) => updateEmployees(e.target.value)}
               >
                 {r}
               </option>
             )) :
               null}
          </Form.Control>
          <SimpleButton onClick={() => reassignEmployees()}>
            Assign
          </SimpleButton>
          <SimpleButton onClick={() => history.goBack()}>Back</SimpleButton>
        </Form.Group>
      </>
    </div>
  );
};

export default withRouter(IndividualProject);
