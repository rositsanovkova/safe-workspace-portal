import React, { useState } from 'react';
import { Form } from 'react-bootstrap';
import { BASE_URL } from '../../../common/Constant';
import SimpleButton from '../../../hoc/SimpleButton/SimpleButton';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { notify } from '../../../common/Notification';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 160,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1 ?
       theme.typography.fontWeightRegular :
        theme.typography.fontWeightMedium,
  };
}

const CreateProject = (props) => {
  const history = props.history;
  const companyId = props.location.pathname.slice(1, 2);
  console.log(companyId);

  const [country, setCountry] = useState([]);
  const [availableEmployees, setAvailableEmployees] = useState([]);
  const [assignEmployees, setAssignEmployees] = useState([]);

  const classes = useStyles();
  const theme = useTheme();

  const [project, setProject] = useState({
    title: {
      value: '',
      touched: false,
      valid: true,
    },
    employees: {
      value: [],
      touched: false,
      valid: true,
    },
  });

  const updateProject = (prop, value) =>
    setProject({
      ...project,
      [prop]: {
        value,
        touched: true,
        valid: projectValidators[prop].reduce(
            (isValid, validatorFn) =>
              isValid && typeof validatorFn(value) !== 'string',
            true
        ),
      },
    });

  const handleChange = (event) => {
    setAssignEmployees(event.target.value);
  };

  const projectValidators = {
    title: [
      (value) => value?.length >= 1 || `Title should be at least 1 letters.`,
      (value) =>
        value?.length <= 30 || `Title should be no more than 30 letters.`,
    ],
    employees: [
      (value) => value?.length >= 4 || `Employee should be at least 4 letters.`,
      (value) =>
        value?.length <= 20 || `Employee should be no more than 20 letters.`,
    ],
  };

  const getValidationErrors = (prop) => {
    return projectValidators[prop]
        .map((validatorFn) => validatorFn(project[prop].value)) // [string, true, true, string]
        .filter((value) => typeof value === 'string');
  };

  const renderValidationError = (prop) =>
    project[prop].touched && !project[prop].valid ?
    getValidationErrors(prop).map((error, index) => (
      <p className="error" key={index}>
        {error}
      </p>
    )) :
     null;

  const getClassNames = (prop) => {
    let classes = '';
    if (project[prop].touched) {
      classes += 'touched';
    }
    if (project[prop].valid === true) {
      classes += 'valid';
    }
    if (project[prop].valid === false) {
      classes += 'invalid';
    }
    return classes;
  };

  const getEmployees = () => {
    fetch(`${BASE_URL}/employees/available/${companyId}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return alert(result.message);
          }
          if (result.length === 0) {
            return notify('No more employees are available');
          }
          const allEmployees = result.map((r) => r.user.fullName);
          setAvailableEmployees(allEmployees);
        })
        .catch(alert);
  };

  const create = () => {
    if (!project.title.valid) {
      return alert('Invalid title!');
    }
    if (!project.employees.valid) {
      return alert('Invalid employees!');
    }

    fetch(`${BASE_URL}/project/company/${companyId}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({
        // ...user,
        title: project.title.value,
        employees: assignEmployees,
      }),
    })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return alert(result.message);
          }
          history.push(``);
          history.push(`companies/${result.company.id}`);
        })
        .catch(alert);
  };

  return (
    <div className="create-projec">
      <h5>Create new project: </h5>
      <Form className="form-content-project">
        <Form.Group controlId="form-title-project">
          <Form.Control
            type="text"
            placeholder="&#xF007; Title"
            className={getClassNames('title')}
            value={project.title.value}
            onChange={(e) => updateProject('title', e.target.value)}
          />
          <Form.Text className="text-muted">
            {renderValidationError('title')}
          </Form.Text>
        </Form.Group>

        <FormControl
          className={classes.formControl}
          onClick={() => getEmployees()}
        >
          <InputLabel id="demo-mutiple-name-label">
            Employees select:
          </InputLabel>
          <Select
            labelId="demo-mutiple-name-label"
            id="demo-mutiple-name"
            multiple
            value={assignEmployees}
            onChange={handleChange}
            input={<Input />}
            MenuProps={MenuProps}
          >
            {availableEmployees.length > 0 ?
             availableEmployees.map((r) => (
               <MenuItem
                 key={r}
                 value={r}
                 style={getStyles(availableEmployees, r, theme)}
               >
                 {r}
               </MenuItem>
             )) :
               null}
          </Select>
        </FormControl>
      </Form>
      <SimpleButton onClick={create}>Create</SimpleButton>
      <SimpleButton onClick={() => history.goBack()}>Back</SimpleButton>
    </div>
  );
};

export default CreateProject;
