import React, { useState, useEffect } from 'react';
import { BASE_URL } from '../../../common/Constant';
import CustomPaginationActionsTableProjects from '../../../hoc/TablePaginationProjects/TablePaginationProjects';
import { notify } from '../../../common/Notification';

const AllProjects = (props) => {
  const [projects, setProject] = useState([]);
  const [loading, setLoading] = useState(false);
  // eslint-disable-next-line
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/project`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((projects) => {
          if (projects.message) {
            setProject([]);
            notify(`${projects.message}`);
          } else {
            setProject(projects);
          }
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  }, []);

  return (
    <div className="all-projects-main">
      {loading ? <h1>Loading...</h1> : null}
      <>
        <CustomPaginationActionsTableProjects rows={projects} />
      </>
    </div>
  );
};

export default AllProjects;
