import React, { useState, useEffect, useContext } from 'react';
import { BASE_URL } from '../../../common/Constant';
import AuthContext from '../../../providers/AuthContext';
import SingleCompany from '../SingleCompany/SingleCompany';
import { notify } from '../../../common/Notification';

const AllCompanies = (props) => {
  const [companies, setCompanies] = useState([]);
  const [loading, setLoading] = useState(false);

  const [error, setError] = useState(null);
  const { loggedUser } = useContext(AuthContext);

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/companies`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((companies) => {
          if (companies.message) {
            setCompanies([]);
            notify(`${companies.message}`);
          } else {
            setCompanies(companies);
          }
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  }, []);

  return (
    <div className="all-companies-main">
      {loading ? <h1>Loading...</h1> : null}
      <>
        {companies.length > 0 ?
         companies.map((company) => (
           <SingleCompany
             key={company.id}
             loggedUser={loggedUser}
             company={company}
           />
         )) :
          null}
      </>
    </div>
  );
};

export default AllCompanies;
