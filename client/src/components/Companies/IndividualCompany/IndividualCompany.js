import React, { useEffect, useState, useContext } from 'react';
import { withRouter } from 'react-router-dom';
import AuthContext from '../../../providers/AuthContext';
import { BASE_URL } from '../../../common/Constant';
import CustomPaginationActionsTableProjects from '../../../hoc/TablePaginationProjects/TablePaginationProjects';
import SimpleButton from '../../../hoc/SimpleButton/SimpleButton';
import { notify } from '../../../common/Notification';

const IndividualCompany = (props) => {
  const match = props.match;
  const { loggedUser } = useContext(AuthContext);
  const [loading, setLoading] = useState(false);
  // eslint-disable-next-line
  const [error, setError] = useState(null);
  const [company, setCompany] = useState({
    country: '',
    projects: [],
    floorPlanning: '',
    employees: [],
  });

  const [cases, setCases] = useState('');

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/companies/${match.params.id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((response) => response.json())
        .then((company) => {
          if (company.message) {
            notify(`${company.message}`);
          }
          setCompany(company);
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  }, [match.params.id, cases]);

  const getDailyCases = () => {
    fetch(
        `https://coronavirus-19-api.herokuapp.com/countries/${company.country}`
    )
        .then(function(res) {
          return res.json();
        })
        .then(async (result) => {
          setCases(result.todayCases);
        })
        .catch();
  };

  return (
    <div className="individual-company-main">
      {loading ? <h1>Loading...</h1> : null}
      <>
        {loggedUser.role === 2 ? (
          <SimpleButton
            onClick={() => props.history.push(`/${company.id}/project/create`)}
          >
            New Project
          </SimpleButton>
        ) : null}
        <SimpleButton
          onClick={() => props.history.push(`/${company.id}/floor-planning`)}
        >
          GoToFloorScheme
        </SimpleButton>
        <SimpleButton onClick={() => getDailyCases()}>
          GetDailyCases
        </SimpleButton>{' '}
        {cases === '' ? null : cases}
        {loggedUser.role === 2 ? (
          <CustomPaginationActionsTableProjects rows={company.projects} />
        ) : null}
      </>
    </div>
  );
};

export default withRouter(IndividualCompany);
