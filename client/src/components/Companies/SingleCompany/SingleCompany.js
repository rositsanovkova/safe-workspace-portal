import React, { useContext, useState, createRef } from 'react';
import propTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import AuthContext from '../../../providers/AuthContext';

import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CircularProgressWithLabel from '../../../hoc/CircularProgress/CircularProgrees';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '95%',
    margin: '2px',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

const SingleCompany = (props) => {
  const company = props.company;
  // eslint-disable-next-line
  const { loggedUser } = useContext(AuthContext);
  const classes = useStyles();
  const [expanded, setExpanded] = useState(false);

  const handleChange = () => {
    setExpanded(expanded ? false : true);
  };

  const myRef = createRef();
  const accordionRef = createRef();

  return (
    <div ref={myRef} className={classes.root}>
      <Accordion
        ref={accordionRef}
        expanded={expanded}
        onClick={() => handleChange()}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography className={classes.heading}>Country: {company.country}</Typography>
          <Typography className={classes.secondaryHeading}>FloorPlanning: {company.floorPlanning ? <span style={{ color: '#8dc63f' }}>available</span> : <span style={{ color: '#f26725' }}>not available</span>}</Typography>
        </AccordionSummary>

        <hr className="single-company-hr"></hr>
        <AccordionDetails onClick={() => props.history.push(`/companies/${company.id}`)}>

          <div className={classes.column}>
            <Typography>Projects: {company.projects.length}</Typography>
            <Typography >Employees: {company.employees.length}</Typography>
          </div>
          <div className="single-company-ratio">
            <div className={classes.column}>
              <Typography >Desks: {company.floorPlanning ? company.floorPlanning.numberOfDesks : null} </Typography>
              <Typography >Ratio: </Typography>
              <CircularProgressWithLabel value={Math.round(company.ratio * 100)} ratio={company.ratio}/>
            </div>
          </div>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

SingleCompany.propTypes = {
  company: propTypes.object.isRequired,
};

export default withRouter(SingleCompany);
