import React, { useState, useEffect } from 'react';
import { BASE_URL } from '../../../common/Constant';
import CustomPaginationActionsTableVacations from '../../../hoc/TablePaginationVacations/TablePaginationVacations';
import { notify } from '../../../common/Notification';

const AllVacations = () => {
  const [vacations, setVacations] = useState([]);
  const [loading, setLoading] = useState(false);
  // eslint-disable-next-line
  const [error, setError] = useState(null);
  const [status, setStatus] = useState('');

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/vacations`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((vacations) => {
          if (vacations.message) {
            setVacations([]);
            notify(`No vacations!`);
          } else {
            setVacations(vacations);
          }
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  }, [status]);

  const approve = (vacationId) => {
    setLoading(true);
    fetch(`${BASE_URL}/vacations/${vacationId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({ status: 2 }),
    })
        .then((response) => response.json())
        .then((result) => {
          if (result.error) {
            throw new Error(result.message);
          }
          setStatus(result.status);
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  };

  const reject = (vacationId) => {
    setLoading(true);
    fetch(`${BASE_URL}/vacations/${vacationId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({ status: 3 }),
    })
        .then((response) => response.json())
        .then((result) => {
          if (result.error) {
            throw new Error(result.message);
          }
          setStatus(result.status);
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  };

  return (
    <div>
      {loading ? <h1>Loading...</h1> : null}
      <>
        <CustomPaginationActionsTableVacations
          rows={vacations}
          reject={reject}
          approve={approve}
        />
      </>
    </div>
  );
};

export default AllVacations;
