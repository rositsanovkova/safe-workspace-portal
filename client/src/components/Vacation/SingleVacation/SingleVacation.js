/* eslint-disable no-undefined */
import React from 'react';
import propTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import SimpleButton from '../../../hoc/SimpleButton/SimpleButton';

const SingleVacation = (props) => {
  const location = props.location;
  const vacation = props.vacation;
  const cancel = props.cancel;
  const approve = props.approve;
  const reject = props.reject;

  const isProfile = location.pathname.includes('profile');
  const isVacation = location.pathname.includes('vacations');

  const getStatus = (status) => {
    let vacationStatus;
    switch (status) {
      case 2:
        vacationStatus = 'Approved';
        break;
      case 3:
        vacationStatus = 'Rejected';
        break;
      case 4:
        vacationStatus = 'Canceled';
        break;
      default:
        vacationStatus = undefined;
        break;
    }
    return vacationStatus;
  };

  return (
    <div className='vacations'>
      <p>
        {vacation ? `${vacation.startDate} - ${vacation.endDate} ${vacation.status === 1 ? 'Pending' : `: ${getStatus(vacation.status)}`}` : null}
        {isVacation ? ` ${vacation.user.fullName} ${vacation.user.country}` : null}
      </p>
      {
        isProfile && vacation.status === 1 ?
        <SimpleButton onClick={() => cancel(vacation.id)}>Cancel</SimpleButton> :
         null
      }

      {isVacation && vacation.status === 1 ? (
        <>
          <SimpleButton style={{ width: '10px' }} onClick={()=> approve(vacation.id)}>Approve</SimpleButton>
          <SimpleButton style={{ width: '10px' }} onClick={()=> reject(vacation.id)}>Reject</SimpleButton>
        </>) :
         null
      }
    </div>
  );
};

SingleVacation.propTypes = {
  vacation: propTypes.object.isRequired,
};

export default withRouter(SingleVacation);
