import React, { useState, useContext } from 'react';
import { Form } from 'react-bootstrap';
import { BASE_URL } from '../../common/Constant';
import AuthContext, { extractUser } from '../../providers/AuthContext';

import SimpleButton from '../../hoc/SimpleButton/SimpleButton';

import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBInput,
} from 'mdbreact';

import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const SignIn = (props) => {
  const history = props.history;
  const location = props.location;
  // eslint-disable-next-line
  const classes = useStyles();
  // eslint-disable-next-line
  const { isLoggedIn, loggedUser, setLoginState } = useContext(AuthContext);

  const [confirmPass, setConfirmPass] = useState({
    confirmPass: {
      value: '',
      touched: false,
      valid: true,
    },
  });

  const [country, setCountry] = useState([]);
  // eslint-disable-next-line
  const [chosenCountry, setChosenCountry] = useState('');

  const [user, setUserObject] = useState({
    username: {
      value: '',
      touched: false,
      valid: true,
    },
    password: {
      value: '',
      touched: false,
      valid: true,
    },
    confirmPass: {
      value: '',
      touched: false,
      valid: true,
    },
    fullName: {
      value: '',
      touched: false,
      valid: true,
    },
    mail: {
      value: '',
      touched: false,
      valid: true,
    },
    country: {
      value: '',
      touched: false,
      valid: true,
    },
  });

  const updateUser = (prop, value) =>
    setUserObject({
      ...user,
      [prop]: {
        value,
        touched: true,
        valid: userValidators[prop].reduce(
            (isValid, validatorFn) =>
              isValid && typeof validatorFn(value) !== 'string',
            true
        ),
      },
    });

  const updateConfirmPass = (prop, value) =>
    setConfirmPass({
      ...confirmPass,
      [prop]: {
        value,
        touched: true,
        valid: userValidators[prop].reduce(
            (isValid, validatorFn) =>
              isValid && typeof validatorFn(value) !== 'string',
            true
        ),
      },
    });

  const userValidators = {
    username: [
      (value) => value?.length >= 4 || `Username should be at least 4 letters.`,
      (value) =>
        value?.length <= 20 || `Username should be no more than 20 letters.`,
    ],
    password: [
      (value) => value?.length >= 4 || `Password should be at least 4 letters.`,
      (value) =>
        value?.length <= 20 || `Password should be no more than 20 letters.`,
      (value) =>
        /[a-zA-Z]/.test(value) ||
        `Password should contain at least one letter.`,
      (value) =>
        /[0-9]/.test(value) || `Password should contain at least one number.`,
    ],
    confirmPass: [
      (value) => value?.length >= 4 || `Password should be at least 4 letters.`,
      (value) =>
        value?.length <= 20 || `Password should be no more than 20 letters.`,
      (value) =>
        /[a-zA-Z]/.test(value) ||
        `Password should contain at least one letter.`,
      (value) =>
        /[0-9]/.test(value) || `Password should contain at least one number.`,
    ],
    fullName: [
      (value) => value?.length >= 4 || `Fullname should be at least 4 letters.`,
      (value) =>
        value?.length <= 30 || `Fullname should be no more than 30 letters.`,
    ],
    mail: [
      (value) => value?.length >= 4 || `Mail should be at least 4 letters.`,
      (value) =>
        // eslint-disable-next-line no-useless-escape
        /^(([^<>()[\]{}'^?\\.,!|//#%*-+=&;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(value) || `Mail should contain at least one letter.`,
    ],
    country: [
      (value) => value?.length >= 4 || `Country should be at least 4 letters.`,
      (value) =>
        value?.length <= 30 || `Country should be no more than 30 letters.`,
    ],
  };

  const getValidationErrors = (prop) => {
    return userValidators[prop]
        .map((validatorFn) => validatorFn(user[prop].value)) // [string, true, true, string]
        .filter((value) => typeof value === 'string');
  };

  const renderValidationError = (prop) =>
    user[prop].touched && !user[prop].valid ?
     getValidationErrors(prop).map((error, index) => (
       <p className="error" key={index}>
         {error}
       </p>
     )) :
      null;
  // eslint-disable-next-line
  const getClassNames = (prop) => {
    let classes = '';
    if (user[prop].touched) {
      classes += 'touched';
    }
    if (user[prop].valid === true) {
      classes += 'valid';
    }
    if (user[prop].valid === false) {
      classes += 'invalid';
    }
    return classes;
  };

  const isLogin = location.pathname.includes('login');

  const login = () => {
    if (!user.username.valid) {
      return alert('Invalid username!');
    }
    if (!user.password.valid) {
      return alert('Invalid password!');
    }
    if (user.username.value.includes('@')) {
      fetch(`${BASE_URL}/session/mail`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          mail: user.username.value,
          password: user.password.value,
        }),
      })
          .then((r) => r.json())
          .then((result) => {
            if (result.error) {
              return alert(result.message);
            }
            try {
              setLoginState({
                isLoggedIn: true,
                loggedUser: extractUser(result.token),
              });
            } catch (e) {
              return alert(e.message.constraints);
            }

            localStorage.setItem('token', result.token);
            history.push('/home');
          })
          .catch(alert);
    } else {
      fetch(`${BASE_URL}/session/username`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: user.username.value,
          password: user.password.value,
        }),
      })
          .then((r) => r.json())
          .then((result) => {
            if (result.error) {
              return alert(result.message);
            }
            try {
              setLoginState({
                isLoggedIn: true,
                loggedUser: extractUser(result.token),
              });
            } catch (e) {
              return alert(e.message.constraints);
            }

            localStorage.setItem('token', result.token);
            history.push('/home');
          })
          .catch(alert);
    }
  };

  const register = () => {
    if (!user.username.valid) {
      return alert('Invalid username!');
    }
    if (!user.password.valid) {
      return alert('Invalid password!');
    }
    if (user.password.value !== confirmPass.confirmPass.value) {
      return alert('Passwords doesn\'t match !');
    }
    if (!user.fullName.valid) {
      return alert('Invalid fullname!');
    }
    if (!user.mail.valid) {
      return alert('Invalid mail!');
    }
    if (!user.country.valid) {
      return alert('Invalid country!');
    }

    fetch(`${BASE_URL}/users`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: user.username.value,
        password: user.password.value,
        fullName: user.fullName.value,
        mail: user.mail.value,
        country: user.country.value,
      }),
    })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return alert(result.message);
          }

          history.push('/login');
        })
        .catch(alert);
  };

  const getCountry = () => {
    fetch(`${BASE_URL}/countries`)
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return alert(result.message);
          }
          const allCountries = result.map((r) => r.name);
          setCountry(allCountries);
        })
        .catch(alert);
  };

  return (
    <div className="register-main">
      <MDBContainer>
        <MDBRow>
          <MDBCol md="6">
            <MDBCard>
              <MDBCardBody className="mx-4">
                <div className="text-center">
                  <h3 className="dark-grey-text mb-5">
                    <strong>{isLogin ? 'SignIn' : 'Register'}</strong>
                  </h3>
                </div>
                {
                isLogin ?
                 <MDBInput
                   label="Username/Email"
                   group
                   type="name"
                   validate
                   error="wrong"
                   success="right"
                   value={user.username.value}
                   className={user.username.touched ? user.username.valid ? 'is-valid' : 'is-invalid' : null}
                   onChange={(e) => updateUser('username', e.target.value)}
                   id="register"
                 /> :
                  <MDBInput
                    label="Username"
                    group
                    type="name"
                    validate
                    error="wrong"
                    success="right"
                    value={user.username.value}
                    className={user.username.touched ? user.username.valid ? 'is-valid' : 'is-invalid' : null}
                    onChange={(e) => updateUser('username', e.target.value)}
                    id="register"
                  />
                }
                <Form.Text className="text-muted">
                  {renderValidationError('username')}
                </Form.Text>
                <MDBInput
                  label="Your password"
                  group
                  type="password"
                  validate
                  containerClass="mb-0"
                  error="wrong"
                  success="right"
                  value={user.password.value}
                  className={user.password.touched ? user.password.valid ? 'is-valid' : 'is-invalid' : null}
                  onChange={(e) => updateUser('password', e.target.value)}
                />
                <Form.Text className="text-muted">
                  {renderValidationError('password')}
                </Form.Text>
                {isLogin ? (
                  <SimpleButton onClick={login}>Login</SimpleButton>
                ) : (
                  <>
                    <MDBInput
                      label="Confirm password"
                      group
                      type="password"
                      validate
                      containerClass="mb-0"
                      error="wrong"
                      success="right"
                      value={confirmPass.confirmPass.value}
                      className={confirmPass.confirmPass.touched ? confirmPass.confirmPass.valid ? 'is-valid' : 'is-invalid' : null}
                      onChange={(e) => updateConfirmPass('confirmPass', e.target.value)}
                    />
                    <Form.Text className="text-muted">
                      {renderValidationError('confirmPass')}
                    </Form.Text>

                    <MDBInput
                      label="Fullname"
                      group
                      type="name"
                      validate
                      error="wrong"
                      success="right"
                      value={user.fullName.value}
                      className={user.fullName.touched ? user.fullName.valid ? 'is-valid' : 'is-invalid' : null}
                      onChange={(e) => updateUser('fullName', e.target.value)}
                      id="formBasicFullName"
                    />
                    <Form.Text className="text-muted">
                      {renderValidationError('fullName')}
                    </Form.Text>
                    <MDBInput
                      label="Mail"
                      group
                      type="mail"
                      validate
                      error="wrong"
                      success="right"
                      value={user.mail.value}
                      className={user.mail.touched ? user.mail.valid ? 'is-valid' : 'is-invalid' : null}
                      onChange={(e) => updateUser('mail', e.target.value)}
                      id="formBasicMail"
                    />
                    <Form.Text className="text-muted">
                      {renderValidationError('mail')}
                    </Form.Text>
                    <Form.Group controlId="formBasicCountry">
                      <Form.Control
                        as="select"
                        onClick={() => getCountry()}
                        onChange={(e) => updateUser('country', e.target.value)}
                      >
                        <option>Country select:</option>
                        {country.length > 0 ?
                         country.map((r) => <option key={r}>{r}</option>) :
                         null}
                      </Form.Control>
                      <Form.Text className="text-muted">
                        {renderValidationError('country')}
                      </Form.Text>
                    </Form.Group>
                    <SimpleButton onClick={register}>Register</SimpleButton>
                  </>
                )}
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </div>
  );
};

export default SignIn;
