import React from 'react';
import Footer from '../Footer/Footer';
import video from '../../images/video.mp4';

const Home = () => {
  return (
    <div>
      <div className="home-page-video">
        <video controls autostart={false} src={video} type="video/mp4" style={{ height: '550px', width: '900px' }} />
      </div>
      <Footer></Footer>
    </div>
  );
};

export default Home;
