import React, { useContext } from 'react';
import { Link, withRouter } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import { ProSidebar, Menu, MenuItem } from 'react-pro-sidebar';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import BusinessIcon from '@material-ui/icons/Business';
import PeopleIcon from '@material-ui/icons/People';
import FolderIcon from '@material-ui/icons/Folder';
import PublicIcon from '@material-ui/icons/Public';
import SpaIcon from '@material-ui/icons/Spa';
import './custom.scss';

const Sidebar = () => {
  // eslint-disable-next-line
  const { isLoggedIn, loggedUser, setLoginState } = useContext(AuthContext);

  return (
    <>
      {isLoggedIn && loggedUser.role === 2 ? (
              <div className="navigation-sidebar" style={{ height: '100%' }}>
                <ProSidebar>
                  <Menu iconShape="square">
                    <MenuItem></MenuItem>
                    <MenuItem icon={<BusinessIcon style={{ backgroundColor: '#00aec5' }}/>} ><Link to="/companies"></Link>Companies</MenuItem>
                    <MenuItem icon={<PublicIcon />}><Link to="/countries"></Link>Country Ratios</MenuItem>
                    <MenuItem icon={<HomeWorkIcon />}><Link to="/floor-planning/create"></Link>Create Workspace</MenuItem>
                    <MenuItem icon={<FolderIcon />}><Link to="/project"></Link>Projects</MenuItem>
                    <MenuItem icon={<PeopleIcon />}><Link to="/employees"></Link>Employees</MenuItem>
                    <MenuItem icon={<SpaIcon />}><Link to="/vacations"></Link>Vacations</MenuItem>
                  </Menu>
                </ProSidebar>
              </div>
          ) : null}
    </>
  )
};

export default withRouter(Sidebar);
