/* eslint-disable no-sequences */
import React, { useContext } from 'react';
import { withRouter } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import 'react-pro-sidebar/dist/css/styles.css';
import { Navbar, Nav } from 'react-bootstrap';

import safeportalworkspace3 from '../../images/safeportalworkspace3.png';

const Navigation = (props) => {
  const { isLoggedIn, loggedUser, setLoginState } = useContext(AuthContext);

  const logout = () => {
    setLoginState({ isLoggedIn: false, loggedUser: null });
    localStorage.removeItem('token');
    props.history.push('/home');
  };

  return (
    <div>

      <Navbar className="navbar-color" variant="light">
        <Navbar.Brand href="#" onClick={() => props.history.push('/')}>
          <img
            src={safeportalworkspace3}
            width="170"
            height="50"
            className="d-inline-block align-top"
            alt="React Bootstrap logo"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          {isLoggedIn ? (
        <>
          <Nav className="ml-auto">
            <Nav.Link id="navbar-href" href="#" onClick={() => props.history.push('/')}>Home</Nav.Link>
            { loggedUser.role === 1 ?
                <Nav.Link id="navbar-href" href="#" onClick={() => props.history.push('/companies')}>Companies</Nav.Link> :
                 null }
            <Nav.Link id="navbar-href" href="#" onClick={() => props.history.push('/profile')}>Profile</Nav.Link>
            <Nav.Link id="navbar-logout" href="#" onClick={() => (logout(), props.history.push('/logout'))}>Logout</Nav.Link>
          </Nav>
        </>
      ) : (
        <>
          <Nav className="ml-auto">
            <Nav.Link id="navbar-href" href="#" onClick={() => props.history.push('/login')}>Login</Nav.Link>
            <Nav.Link id="navbar-href" href="#" onClick={() => props.history.push('/register')}>Register</Nav.Link>
          </Nav>
        </>
      )}
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
};

export default withRouter(Navigation);
