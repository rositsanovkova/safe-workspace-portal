import React, { useState, useEffect } from 'react';
import { BASE_URL } from '../../common/Constant';
import EditableTableRatios from '../../hoc/EditableTableRatios/EditableTableRatios';
import { notify } from '../../common/Notification';

const AllCountries = (props) => {
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [length, setLength] = useState(0);

  let data = [];
  useEffect(() => {
    fetch(`${BASE_URL}/countries`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((countries) => {
          if (countries.message) {
            setCountries('');
            notify(`${countries.message}`);
          } else {
            countries.forEach((country) => {
              data.push({
                name: country.name,
                minRatio: +country.minRatio * 100,
                maxRatio: +country.maxRatio * 100,
              });
            });

            setLength(countries.length);
          }
        })
        .catch((error) => setError(error.message));
  }, [countries, data]);

  return (
    <div className="all-companies-main">
      {loading ? <h1>Loading...</h1> : null}
      <>
        <EditableTableRatios rows={data} />
      </>
    </div>
  );
};

export default AllCountries;
