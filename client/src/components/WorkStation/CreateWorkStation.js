import React, { useState, useContext } from 'react';
import { Form } from 'react-bootstrap';
import { BASE_URL } from '../../common/Constant'
import AuthContext from '../../providers/AuthContext';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';

const CreateWorkStation = (props) => {
  const history = props.history;
  const floorPlanningId = history.location.pathname.split('/')[1]
  // eslint-disable-next-line
  const { isLoggedIn, loggedUser, setLoginState } = useContext(AuthContext);
  const [workStation, setWorkStation] = useState({
    numberOfDesks: {
      value: 0,
      touched: false,
      valid: true,
    },
    rowDesks: {
      value: 0,
      touched: false,
      valid: true,
    },
    columnDesks: {
      value: 0,
      touched: false,
      valid: true,
    },
  })


  const updateWorkStation = (prop, value) =>
    setWorkStation({
      ...workStation,
      [prop]: {
        value,
        touched: true,
        valid: workStationValidators[prop].reduce(
            (isValid, validatorFn) =>
              isValid && typeof validatorFn(value) !== 'string',
            true
        ),
      },
    });

  const workStationValidators = {
    numberOfDesks: [
      (value) => value >= 1 || `There should be at least one desk!`,
    ],
    rowDesks: [
      (value) => value >= 1 || `There should be at least one desk!`,
    ],
    columnDesks: [
      (value) => value >= 1 || `There should be at least one desk!`,
    ],
  };

  const getValidationErrors = (prop) => {
    return workStationValidators[prop]
        .map((validatorFn) => validatorFn(workStation[prop].value)) // [string, true, true, string]
        .filter((value) => typeof value === 'string');
  };

  const renderValidationError = (prop) =>
        workStation[prop].touched && !workStation[prop].valid ?
        getValidationErrors(prop).map((error, index) => (
          <p className="error" key={index}>
            {error}
          </p>
        )) :
        null;

  const getClassNames = (prop) => {
    let classes = '';
    if (workStation[prop].touched) {
      classes += 'touched';
    }
    if (workStation[prop].valid === true) {
      classes += 'valid';
    }
    if (workStation[prop].valid === false) {
      classes += 'invalid';
    }
    return classes;
  };


  const create = () => {
    console.log(floorPlanningId)
    if (!workStation.rowDesks.valid || workStation.rowDesks.value === 0 || workStation.rowDesks.value === '') {
      return alert('Invalid row desks entered!');
    }
    if (!workStation.columnDesks.valid || workStation.columnDesks.value === 0) {
      return alert('Invalid column desks entered!');
    }

    fetch(`${BASE_URL}/workstations`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },

      body: JSON.stringify({
        // ...user,
        rowDesks: workStation.rowDesks.value,
        columnDesks: workStation.columnDesks.value,
        floorPlanning: floorPlanningId,
      }),
    })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return alert(result.message);
          }
          history.push(``)
          history.push(`${floorPlanningId}/floor-planning`)
        })
        .catch(alert);
  };


  return (
    <>
      <Form className="form-content-workstation">
        <Form.Group controlId="form-row-desks-workstation">
          <Form.Label>
            <b>Row Desks: </b>
          </Form.Label>
          <Form.Control
            type="number"
            placeholder="&#xF007; Row Desks"
            className={getClassNames('rowDesks')}
            value={workStation.rowDesks.value}
            onChange={(e) => updateWorkStation('rowDesks', e.target.value)}
          />
          <Form.Text className="text-muted">
            {renderValidationError('rowDesks')}
          </Form.Text>
        </Form.Group>
        <Form.Group controlId="form-columnDesks-workstation">
          <Form.Label>
            <b>Column Desks: </b>
          </Form.Label>
          <Form.Control
            type="number"
            placeholder="&#xF007; Column Desks"
            className={getClassNames('columnDesks')}
            value={workStation.columnDesks.value}
            onChange={(e) => updateWorkStation('columnDesks', e.target.value)}
          />
          <Form.Text className="text-muted">
            {renderValidationError('columnDesks')}
          </Form.Text>
        </Form.Group>
      </Form>
      <SimpleButton onClick={create}>Create</SimpleButton>
      <SimpleButton onClick={() => history.goBack()}>Back</SimpleButton>
    </>
  );
};

export default CreateWorkStation;
