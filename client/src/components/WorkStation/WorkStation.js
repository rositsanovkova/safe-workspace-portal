// eslint-disable-next-line
import Konva from 'konva';
import React, { useState } from 'react';
import { Stage, Layer, Text, Rect } from 'react-konva';

const WorkStation = (props) => {
  const workstation = props.workstation;
  const floorPlanning = props.floorPlanning;
  // eslint-disable-next-line
  const numberOfDesks = workstation.rowDesks * workstation.columnDesks;
  // eslint-disable-next-line
  const [desks, setDesks] = useState(workstation.desks);
  let currentEmployees = props.currentEmployees;

  const rectangles = [];
  let change = false;
  let count = 0;

  if (floorPlanning.company.ratio >= 0.05) {
    for (let row = 1; row <= workstation.rowDesks; row++) {
      for (let col = 1; col <= workstation.columnDesks; col++) {
        if (workstation.columnDesks % 2 === 0) {
          change = row % 2 === 0;
        }
        console.log(count)
        rectangles.push(<>
          <Rect
            key={workstation.desks[count].id}
            id={count}
            x={col * 105 + 5}
            y={row * 105 + 40}
            width={100}
            height={100}
            fill={change ? count % 2 === 0 ? '#fb4f46' : '#a6db65' : count % 2 === 0 ? '#a6db65' : '#fb4f46'}
            shadowBlur={10}
            innerRadius={20}
            outerRadius={40}
            shadowColor="black"
            shadowOpacity={0.6}
            desks={desks}
            currentEmployeeID={currentEmployees}
          />
          <Text
            key={`text-${workstation.desks[count].id}`}
            x={col * 105 - 5}
            y={row * 105 + 50}
            currentEmployeeID={currentEmployees}
            width= {140}
            text={workstation.desks[count].status === 3 ? workstation.desks[count].employee.user.fullName : workstation.desks[count].status === 2 ? 'forbidden' : 'available'
            }
            padding={30}
            fontSize={15}
            fontFamily= "Lucida Sans Unicode" />
        </>)
        change = false;
        count++;
      }
    }
  } else {
    for (let row = 1; row <= workstation.rowDesks; row++) {
      for (let col = 1; col <= workstation.columnDesks; col++) {
        if (workstation.columnDesks % 2 === 0) {
          change = row % 2 === 0;
        }
        rectangles.push(<>
          <Rect
            key={workstation.desks[count].id}
            id={count}
            x={col * 105 + 5}
            y={row * 105 + 40}
            width={100}
            height={100}
            fill={'#a6db65'}
            shadowBlur={10}
            innerRadius={20}
            outerRadius={40}
            shadowColor="black"
            shadowOpacity={0.6}
            desks={desks}
            currentEmployeeID={currentEmployees}
          />
          <Text
            key={`text-${workstation.desks[count].id}`}
            x={col * 105 - 5}
            y={row * 105 + 50}
            currentEmployeeID={currentEmployees}
            width= {140}
            fontSize={15}
            fontFamily= "Lucida Sans Unicode"
            text={workstation.desks[count].status === 3 ? workstation.desks[count].employee.user.fullName : workstation.desks[count].status === 2 ? 'forbidden' : 'available'}
            padding={30} />
        </>)
        change = false;
        count++;
      }
    }
  }

  return (
    <Stage
      width={
        100 *
        (workstation.columnDesks === 1 ? 2 : workstation.columnDesks) *
        1.6
      }
      height={
        100 * (workstation.rowDesks === 1 ? 2 : workstation.rowDesks) * 1.8
      }
      padding={0}
      margin={0}
    >
      <Layer key={workstation.id}>{rectangles}</Layer>
    </Stage>
  );
};

export default WorkStation;
