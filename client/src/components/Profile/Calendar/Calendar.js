import React, { useState } from 'react';
import ReactLightCalendar from '@lls/react-light-calendar'
import '@lls/react-light-calendar/dist/index.css'
import { BASE_URL } from '../../../common/Constant';
import moment from 'moment';
import SimpleButton from '../../../hoc/SimpleButton/SimpleButton';

const VacationCalendar = () => {
  const date = new Date()
  const [vacation, setVacation] = useState({
    startDate: date.getTime(),
    endDate: new Date(date.getTime()).setDate(date.getDate() + 6),
  })

  const handleChange = (startDate, endDate) => {
    setVacation({
      startDate: startDate,
      endDate: endDate,
    })
  }

  const createVacation = () => {
    console.log(vacation)
    if (vacation.endDate===null) {
      vacation.endDate = vacation.startDate
    }
    fetch(`${BASE_URL}/vacations`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({
        startDate: moment(vacation.startDate).format('MMM Do YY'),
        endDate: moment(vacation.endDate).format('MMM Do YY'),
      }),
    })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return alert(result.message);
          }
        })
        .catch(alert);
  }

  return (
    <div className="calendar">
      <ReactLightCalendar style={{ color: 'red' }} startDate={vacation.startDate} endDate={vacation.endDate} onChange={handleChange} range displayTime />
      <SimpleButton onClick={() => createVacation()}>Request Vacation</SimpleButton>
    </div>
  )
};

export default VacationCalendar;
