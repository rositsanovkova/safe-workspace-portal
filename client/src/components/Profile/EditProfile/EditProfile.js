import React, { useState, useEffect } from 'react';
import { BASE_URL } from '../../../common/Constant';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import SimpleButton from '../../../hoc/SimpleButton/SimpleButton';
import { notify } from '../../../common/Notification';

const EditProfile = ({ history, location, match }) => {
  const id = match.params.id;
  const [country, setCountry] = useState([]);

  const [user, setUser] = useState({
    fullName: {
      value: '',
      touched: false,
      valid: true,
    },
    mail: {
      value: '',
      touched: false,
      valid: true,
    },
    country: {
      value: '',
      touched: false,
      valid: true,
    },
    employee: '',
  });
  // eslint-disable-next-line
  const [error, setError] = useState(null);
  // eslint-disable-next-line
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/users/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((user) => {
          if (user.message) {
            notify(`${user.message}`);
          }
          setUser({
            fullName: {
              value: user.fullName,
              touched: false,
              valid: true,
            },
            mail: {
              value: user.mail,
              touched: false,
              valid: true,
            },
            country: {
              value: user.country,
              touched: false,
              valid: true,
            },
            employee: user.employee,
          });
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  }, [id]);

  const updateUserProfile = (prop, value) =>
    setUser({
      ...user,
      [prop]: {
        value: value,
        touched: true,
        valid: profileValidator[prop].reduce(
            (isValid, validatorFn) =>
              isValid && typeof validatorFn(value) !== 'string',
            true
        ),
      },
    });

  const profileValidator = {
    fullName: [
      (value) => value?.length >= 4 || `Fullname should be at least 4 letters.`,
      (value) =>
        value?.length <= 30 || `Fullname should be no more than 30 letters.`,
    ],
    mail: [
      (value) => value?.length >= 4 || `Mail should be at least 4 letters.`,
      (value) =>
        // eslint-disable-next-line no-useless-escape
        /^(([^<>()[\]{}'^?\\.,!|//#%*-+=&;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
            value
        ) || `Mail should contain at least one letter.`,
    ],
    country: [
      (value) => value?.length >= 4 || `Country should be at least 4 letters.`,
      (value) =>
        value?.length <= 30 || `Country should be no more than 30 letters.`,
    ],
  };

  const getValidationErrors = (prop) => {
    return profileValidator[prop]
        .map((validatorFn) => validatorFn(user[prop].value))
        .filter((value) => typeof value === 'string');
  };

  const renderValidationError = (prop) =>
    user[prop].touched && !user[prop].valid ?
     getValidationErrors(prop).map((error, index) => (
       <p className="error" key={index}>
         {error}
       </p>
     )) :
      null;

  const getClassNames = (prop) => {
    let classes = '';
    if (user[prop].touched) {
      classes += 'touched';
    }
    if (user[prop].valid === true) {
      classes += 'valid';
    }
    if (user[prop].valid === false) {
      classes += 'invalid';
    }
    return classes;
  };

  const getCountry = () => {
    fetch(`${BASE_URL}/companies`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            alert(result.message);
          }
          const allCountries = result.map((r) => r.country);
          setCountry(allCountries);
        })
        .catch(alert);
  };

  const updateProfile = () => {
    if (!user.fullName.valid) {
      notify('username is not valid');
    }
    if (!user.mail.valid) {
      notify('mail is not valid');
    }
    if (!user.country.valid) {
      notify('country is not valid');
    }
    setLoading(true);
    fetch(`${BASE_URL}/users/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({
        fullName: user.fullName.value,
        mail: user.mail.value,
        country: user.country.value,
      }),
    })
        .then((response) => response.json())
        .then((result) => {
          if (result.message) {
            notify(`${result.message}`);
          }
          history.goBack();
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  };

  return (
    <div className="form-content">
      <Form>
        <Form.Group controlId="formBasicFullName">
          <Form.Label>Fullname:</Form.Label>
          <Form.Control
            type="text"
            className={getClassNames('fullName')}
            value={user.fullName.value}
            onChange={(e) => updateUserProfile('fullName', e.target.value)}
          />
          <Form.Text className="text-muted">
            {renderValidationError('fullName')}
          </Form.Text>
        </Form.Group>
        <Form.Group controlId="formBasicMail">
          <Form.Label>Mail: </Form.Label>
          <Form.Control
            type="text"
            className={getClassNames('mail')}
            value={user.mail.value}
            onChange={(e) => updateUserProfile('mail', e.target.value)}
          />
          <Form.Text className="text-muted">
            {renderValidationError('mail')}
          </Form.Text>
        </Form.Group>

        {user.employee ? null : (
          <Form.Group controlId="form-country-project">
            <Form.Control
              as="select"
              onClick={() => getCountry()}
              onChange={(e) => updateUserProfile('country', e.target.value)}
            >
              <option>Country select:</option>
              {country.length > 0 ?
               country.map((r) => <option key={r}>{r}</option>) :
                null}
            </Form.Control>
            <Form.Text className="text-muted">
              {renderValidationError('country')}
            </Form.Text>
          </Form.Group>
        )}
      </Form>
      <SimpleButton onClick={updateProfile}>Update</SimpleButton>
      <SimpleButton onClick={() => history.goBack()}>Back</SimpleButton>
    </div>
  );
};

EditProfile.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default withRouter(EditProfile);
