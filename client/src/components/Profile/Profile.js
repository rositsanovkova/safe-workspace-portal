import React, { useContext, useState, useEffect } from 'react';
import AuthContext from '../../providers/AuthContext';
import { BASE_URL } from '../../common/Constant';
import { withRouter } from 'react-router-dom';
import VacationCalendar from './Calendar/Calendar';
import SingleVacation from './../Vacation/SingleVacation/SingleVacation';
import SimpleButton from '../../hoc/SimpleButton/SimpleButton';

const Profile = (props) => {
  const history = props.history;

  const { loggedUser } = useContext(AuthContext);

  const [user, setUser] = useState({
    username: '',
    fullName: '',
    mail: '',
    country: '',
    employee: '',
    vacation: '',
  });

  // eslint-disable-next-line
  const [loading, setLoading] = useState(false);
  // eslint-disable-next-line
  const [error, setError] = useState(null);
  const [status, setStatus] = useState('');
  const [vacation, setVacation] = useState([]);

  const id = loggedUser.id;

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/users/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
        .then((r) => r.json())
        .then((user) => {
          if (user.message) {
            alert(`${user.message}`);
          }

          setUser({
            fullName: user.fullName,
            username: user.username,
            mail: user.mail,
            country: user.country,
            employee: user.employee,
            vacation: user.vacation,
          });
          setVacation(user.vacation)
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  }, [id, status, vacation]);

  const cancel = (vacationId) => {
    setLoading(true);
    fetch(`${BASE_URL}/vacations/${vacationId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({ status: 4 }),
    })
        .then((response) => response.json())
        .then((result) => {
          if (result.error) {
            throw new Error(result.message);
          }
          setStatus(result.status)
        })
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
  }

  return (
    <div className="main-profile">
      <div className="profile-info">
        <div className="profile-user-basic">
          <h3>Username: {user.username}</h3>
          <h3>Fullname: {user.fullName}</h3>
          <h3>Mail: {user.mail}</h3>
          <h3>Company: {user.country}</h3>
          <SimpleButton onClick={() => history.push(`/users/${id}`)}>Edit Profile</SimpleButton>
        </div>
        <br>
        </br>
        <div className="profile-user-emloyee">
          {
        user.employee ?
         <>
           <h3>Employee Project: {user.employee.project ? user.employee.project.title : null}</h3>
           <h3>Employee Current Location: {user.employee.currentLocation === 2 ? 'Office' : 'Home'}</h3>
           <h3>Employee Next Location: {user.employee.nextLocation === 2 ? 'Office' : 'Home'}</h3>
         </> :
          null
          }
        </div>
        <br>
        </br>
        <div className="profile-user-vacations">
          <h3>Vacations:</h3>
          {user.vacation.length > 0 ? (
              user.vacation.map((vacation) => (
                <SingleVacation
                  key={vacation.id}
                  loggedUser={loggedUser}
                  vacation={vacation}
                  cancel={cancel}
                />
              ))
            ) : (
              null
            )}
        </div>
      </div>

      <div className="profile-calendar">
        <VacationCalendar loggedUser={loggedUser}></VacationCalendar>
      </div>
    </div>
  );
};

export default withRouter(Profile);
