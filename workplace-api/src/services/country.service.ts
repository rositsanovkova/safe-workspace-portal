import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { Countries } from 'src/models/countries.entity';
import { UpdateCountryDTO } from 'src/dtos/country/update-coutry.dto';

@Injectable()
export class CountryService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Countries) private readonly countryRepository: Repository<Countries>,

    ) { }

    async getAll(): Promise<Countries[]> {
        const countries = await this.countryRepository.find();
        if (!countries || countries.length === 0) {
            throw new NotFoundException('No companies are found')
        }

        return countries.map(country => country);
    }

    async update(updateCountryDTO: Partial<UpdateCountryDTO>): Promise<void> {
        const country = await this.countryRepository.findOne({ where: { name: updateCountryDTO.name } });

        if (!country) {
            throw new NotFoundException('no such country')
        }

        country.minRatio = +updateCountryDTO.minRatio;
        country.maxRatio = +updateCountryDTO.maxRatio;
        await this.countryRepository.save(country)

    }
}
