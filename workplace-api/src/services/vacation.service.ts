import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { ReturnVacationDTO } from 'src/dtos/vacation/return-vacation.dto';
import { Vacation } from 'src/models/vacation.entity';
import { CreateVacationDTO } from 'src/dtos/vacation/create-vacation.dto';
import { User } from 'src/models/user.entity';
import { UpdateVacationDTO } from 'src/dtos/vacation/update-vacation.dto';

@Injectable()
export class VacationService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Vacation) private readonly vacationRepository: Repository<Vacation>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
    ) { }

    async getAll(): Promise<ReturnVacationDTO[]> {
        const vacations = await this.vacationRepository.find({ relations: ['user'] });
        if (!vacations || vacations.length === 0) {
            throw new NotFoundException('No vacations are found')
        }

        return vacations.map(vacation => this.transformer.toReturnVacationDTO(vacation));
    }

    async getById(vacationId: number): Promise<ReturnVacationDTO> {
        const foundVacation = await this.vacationRepository.findOne({ where: { id: vacationId }, relations: ['user'] })
        if (!foundVacation) {
            throw new NotFoundException('Such vacation does not exist')
        }

        return this.transformer.toReturnVacationDTO(foundVacation);
    }

    async create(vacationDto: CreateVacationDTO, loggedUser: number): Promise<ReturnVacationDTO> {
        const foundLoggedUser = await this.usersRepository.findOneOrFail(loggedUser, { relations: ['vacation'] });
        foundLoggedUser.vacation
            .forEach(item => {
                if (vacationDto.endDate >= item.startDate && vacationDto.startDate <= item.endDate) {
                    throw new BadRequestException('You can\'t request same dates')
                }
            })
        const createdVacation = this.vacationRepository.create();

        if (vacationDto.startDate === vacationDto.endDate) {
            createdVacation.startDate = vacationDto.startDate;
            createdVacation.endDate = vacationDto.startDate;
        } else {
            createdVacation.startDate = vacationDto.startDate;
            createdVacation.endDate = vacationDto.endDate;
        }

        createdVacation.user = foundLoggedUser;
        await this.vacationRepository.save(createdVacation);

        return this.transformer.toReturnVacationDTO(createdVacation);
    }

    async update(updateVacationDto: UpdateVacationDTO, vacationId: number): Promise<ReturnVacationDTO> {
        const vacationFound = await this.vacationRepository.findOne({ where: { id: vacationId } });
        vacationFound.status = updateVacationDto.status;
        await this.vacationRepository.update(vacationFound.id, vacationFound);

        return this.transformer.toReturnVacationDTO(vacationFound);
    }

}
