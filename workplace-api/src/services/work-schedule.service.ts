/* eslint-disable @typescript-eslint/no-unused-vars */
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { WorkSchedule } from 'src/models/work-schedule.entity';
import { ReturnWorkingScheduleDTO } from 'src/dtos/currentEmployee/return-working-schedule.dto';
import { Employee } from 'src/models/employee.entity';
import { Company } from 'src/models/company.entity';
import { Project } from 'src/models/projects.entity';
import { EmployeeLocation } from 'src/enums/employee-location';
import { Countries } from 'src/models/countries.entity';
import { UpdateWorkScheduleDTO } from 'src/dtos/currentEmployee/update-working-schedule.dto'
import { DeskService } from './desk.service';
import { IsNull, Not } from "typeorm";
import { Vacation } from 'src/models/vacation.entity';
import * as moment from 'moment';
import { ReturnEmployeeDTO } from 'src/dtos/employee/return-employee.dto';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';


@Injectable()
export class WorkScheduleService {
    constructor(
        private readonly transformer: TransformService,
        private readonly deskService: DeskService,
        @InjectRepository(WorkSchedule) private readonly workScheduleRepository: Repository<WorkSchedule>,
        @InjectRepository(Employee) private readonly employeeRepository: Repository<Employee>,
        @InjectRepository(Company) private readonly companyRepository: Repository<Company>,
        @InjectRepository(Project) private readonly projectRepository: Repository<Project>,
        @InjectRepository(Countries) private readonly countriesRepository: Repository<Countries>,
        @InjectRepository(Vacation) private readonly vacationRepository: Repository<Vacation>,
    ) { }

    async getAll(): Promise<ReturnWorkingScheduleDTO[]> {
        const workSchedules = await this.workScheduleRepository.find({ relations: ['project', 'employee', 'company', 'employee.company', 'employee.user'] });
        if (!workSchedules) {
            throw new BadRequestException('no workschedules found');
        }
        return workSchedules.map((workschedule) => this.transformer.toReturnWorkScheduleDTO(workschedule))
    }

    async createWorkSchedule(employeesDTO: ReturnEmployeeDTO[]): Promise<ReturnMessageDTO> {
        const mappedEmployees = async () => {
            const foundEmployees = employeesDTO.map((employeeDTO) => {

                return this.employeeRepository.findOne({ where: { id: employeeDTO.id, workSchedule: null }, relations: ['workSchedule', 'company', 'project'] })
            })

            return await Promise.all(foundEmployees)
        }

        const getEmployeeWithoutWorkStations = await mappedEmployees()

        const getEmployeeWithoutWorkStationsMapped = async () => {

            const getThem = getEmployeeWithoutWorkStations.map(async employee => {
                const foundWorkSchedule = await (this.workScheduleRepository.find({ relations: ['employee'] }))
                const getFoundSchedules = foundWorkSchedule.some((workschedule) => workschedule.employee === employee)
                if (!getFoundSchedules) {

                    return employee;
                }
                
            })

            return await Promise.all(getThem)
        }


        const employeesToCreate = await mappedEmployees();

        const employeesToCreate2 = await getEmployeeWithoutWorkStationsMapped()
        const workSchedulesToCreates = employeesToCreate2.filter((employee) => employee !== undefined)

        const forEachEmployeeCreateWorkSChedules = async() => {
            if (workSchedulesToCreates.length > 0) {
                const createdWorkSchedules = workSchedulesToCreates.map((employee) => {

                    const createdWorkSchedule = this.workScheduleRepository.create();
                    createdWorkSchedule.employee = employee;
                    createdWorkSchedule.company = employee.company;
                    createdWorkSchedule.project = employee.project;
                    this.workScheduleRepository.save(createdWorkSchedule);
                    
                    return createdWorkSchedule                
            })
            return await Promise.all(createdWorkSchedules);
            }
            
        }

        const finished = await forEachEmployeeCreateWorkSChedules();
        finished;
        setTimeout(() => {
            const forEachEmployeeAssignWorkSChedules = async () => {
                // console.log(employees)
                const mapped = async () => {
                    const mappedEmployees = employeesToCreate2.map(async (employee) => {

                        const employeeWorkSchedule = await this.workScheduleRepository.findOne({
                            where: { employee: employee }, relations: [
                                'employee',
                                'project',
                                'company',
                                'company.floorPlanning',
                                'company.floorPlanning.workStation'
                            ]
                        });
                        console.log(employeeWorkSchedule)
                        employee.workSchedule = employeeWorkSchedule;

                        return await this.employeeRepository.update(employee.id, employee)
                    })

                    return await Promise.all(mappedEmployees);
                }
                await mapped();
            }
            forEachEmployeeAssignWorkSChedules()
        }, 1000)

        return { message: 'WorkSchedule created' }
    }


    async updateWorkSchedule(workSchedules: ReturnWorkingScheduleDTO[], updateWorkSchedule: UpdateWorkScheduleDTO): Promise<void> {
        const repoCompany = await this.companyRepository.findOne({ where: { id: updateWorkSchedule.company.id }, relations: ['floorPlanning', 'floorPlanning.workStation', 'floorPlanning.workStation.desks', 'floorPlanning.company'] })
        const mappedWorkSchedule = async () => {
            const mapped = workSchedules.map(async (workSchedule) => {
                const foundWorkSchedule = await this.workScheduleRepository.findOne({ where: { id: workSchedule.id } });
                if (updateWorkSchedule.company) {
                    const foundCompany = await this.companyRepository.findOne({ where: { id: updateWorkSchedule.company.id } })
                    foundWorkSchedule.company = foundCompany;
                }
                if (updateWorkSchedule.project || updateWorkSchedule.project === null) {
                    if (updateWorkSchedule.project === null) {
                        foundWorkSchedule.project = null;
                        foundWorkSchedule.currentWeekLocation = EmployeeLocation.Home;
                        foundWorkSchedule.nextWeekLocation = EmployeeLocation.Home;
                    } else {
                        const foundProject = await this.projectRepository.findOne({ where: { id: updateWorkSchedule.project.id } })
                        foundWorkSchedule.project = foundProject;
                    }

                }
                await this.workScheduleRepository.update(foundWorkSchedule.id, foundWorkSchedule);

                return foundWorkSchedule;
            })
            return await Promise.all(mapped);
        }
        await mappedWorkSchedule();
    }

    async employeeSchedule(company: Company) {
        const foundWorkSchedule = await this.workScheduleRepository.find({ where: { company: company, project: Not(IsNull()) }, relations: ['company', 'company.floorPlanning'] });
        let totalNumberOfDesks = 0;

        if (company.floorPlanning !== null) {
            totalNumberOfDesks = company.floorPlanning.numberOfDesks;
        }


        const maxRatio = (await this.countriesRepository.findOne({ where: { name: company.country } })).maxRatio;
        const minRatio = (await this.countriesRepository.findOne({ where: { name: company.country } })).minRatio;

        const vacations = await this.vacationRepository.find({ relations: ['user'] })
        const nextWeekNumber = await this.getWeekNumber(new Date()) + 1;
        const currentWeekNumber = await this.getWeekNumber(new Date());
        const filteredVacations = async () => {
            const vacationsNextWeek = vacations.map(async (vacation) => {
                
                const employee = await this.employeeRepository.findOne({ where: { user: vacation.user } });
                const workSchedule = await this.workScheduleRepository.findOne({ where: { employee: employee } });
                const checkVacationStartDate = await this.getWeekNumber(moment(vacation.startDate, "MMM Do YY").toDate());
                const checkVacationEndDate = await this.getWeekNumber(moment(vacation.endDate, "MMM Do YY").toDate());

                setTimeout(() => {
                    if ((checkVacationStartDate === nextWeekNumber || checkVacationEndDate === nextWeekNumber) && vacation.status === 2) {
                        workSchedule.nextWeekLocation = EmployeeLocation.Vacation;
                        this.workScheduleRepository.update(workSchedule.id, workSchedule)
                        
                        return [workSchedule, 'nextweek'];
                    }

                    if ((checkVacationStartDate === currentWeekNumber || checkVacationEndDate === currentWeekNumber) && vacation.status === 2) {
                        workSchedule.currentWeekLocation = EmployeeLocation.Vacation;
                        this.workScheduleRepository.update(workSchedule.id, workSchedule)
                        
                        return [workSchedule, 'currentweek'];
                    }
                }, 1000)

            })

            return await Promise.all(vacationsNextWeek)
        }

        const vacationToawait = await filteredVacations();
        console.log(vacationToawait)
        if (company.ratio >= minRatio) {
            if (totalNumberOfDesks / 2 < foundWorkSchedule.length) {
                foundWorkSchedule.forEach(async (workSchedule, index) => {
                    if (totalNumberOfDesks !== 0) {

                        if (index <= totalNumberOfDesks / 2 - 1) {
                            if (workSchedule.currentWeekLocation !== EmployeeLocation.Vacation) {
                                workSchedule.currentWeekLocation = EmployeeLocation.Office;
                            }
                            if (workSchedule.nextWeekLocation !== EmployeeLocation.Vacation) {
                                workSchedule.nextWeekLocation = EmployeeLocation.Home;
                            }

                        } else {
                            if (workSchedule.currentWeekLocation !== EmployeeLocation.Vacation) {
                                workSchedule.currentWeekLocation = EmployeeLocation.Home;
                            }
                            if (workSchedule.nextWeekLocation !== EmployeeLocation.Vacation) {
                                workSchedule.nextWeekLocation = EmployeeLocation.Office;
                            }


                        }
                        await this.workScheduleRepository.update(workSchedule.id, workSchedule)
                    }
                })

            } else {
                foundWorkSchedule.forEach(async (workSchedule, index) => {
                    if (totalNumberOfDesks !== 0) {
                        if (workSchedule.currentWeekLocation !== EmployeeLocation.Vacation) {
                            workSchedule.currentWeekLocation = EmployeeLocation.Office;
                        }
                        if (workSchedule.nextWeekLocation !== EmployeeLocation.Vacation) {
                            workSchedule.nextWeekLocation = EmployeeLocation.Office;
                        }



                        await this.workScheduleRepository.update(workSchedule.id, workSchedule)
                    }
                })
            }

            const currentWeek = await this.getWeekNumber(new Date());
            if (currentWeek % 2 !== 0) {
                const currentWorkSchedule = await this.workScheduleRepository.find({ where: { company: company, currentWeekLocation: EmployeeLocation.Office }, relations: ['employee', 'employee.user', 'company', 'company.floorPlanning'] });
                const currentEmployees = currentWorkSchedule.map((workSchedule) => workSchedule.employee)
                const nextWorkSchedule = await this.workScheduleRepository.find({ where: { company: company, nextWeekLocation: EmployeeLocation.Office }, relations: ['employee', 'employee.user', 'company', 'company.floorPlanning'] });
                const nextEmployees = nextWorkSchedule.map((workSchedule) => workSchedule.employee)
                await this.deskService.arrangeDesksCurrentWeek(company.floorPlanning, currentEmployees)
                await this.deskService.arrangeDesksNextWeek(company.floorPlanning, nextEmployees)
            } else {
                const currentWorkSchedule = await this.workScheduleRepository.find({ where: { company: company, nextWeekLocation: EmployeeLocation.Office }, relations: ['employee', 'employee.user', 'company', 'company.floorPlanning'] });
                const currentEmployees = currentWorkSchedule.map((workSchedule) => workSchedule.employee)
                const nextWorkSchedule = await this.workScheduleRepository.find({ where: { company: company, currentWeekLocation: EmployeeLocation.Office }, relations: ['employee', 'employee.user', 'company', 'company.floorPlanning'] });
                const nextEmployees = nextWorkSchedule.map((workSchedule) => workSchedule.employee)
                await this.deskService.arrangeDesksCurrentWeek(company.floorPlanning, currentEmployees)
                await this.deskService.arrangeDesksNextWeek(company.floorPlanning, nextEmployees)
            }
        }

        else if (company.ratio < minRatio) {
            const forEachWorkSchedule = async () => {
                const forEachedWorkScheduled = foundWorkSchedule.map(async (workSchedule) => {
                    if (workSchedule.currentWeekLocation !== EmployeeLocation.Vacation) {
                        workSchedule.currentWeekLocation = EmployeeLocation.Office;
                    }
                    if (workSchedule.nextWeekLocation !== EmployeeLocation.Vacation) {
                        workSchedule.nextWeekLocation = EmployeeLocation.Office;
                    }


                    await this.workScheduleRepository.update(workSchedule.id, workSchedule);

                    return workSchedule;
                })

                return await Promise.all(forEachedWorkScheduled);
            }

            await forEachWorkSchedule();
            const currentWeek = await this.getWeekNumber(new Date());
            if (currentWeek % 2 === 0) {
                const currentWorkSchedule = await this.workScheduleRepository.find({ where: { company: company, currentWeekLocation: EmployeeLocation.Office }, relations: ['employee', 'employee.user', 'company', 'company.floorPlanning'] });
                const currentEmployees = currentWorkSchedule.map((workSchedule) => workSchedule.employee)
                const nextWorkSchedule = await this.workScheduleRepository.find({ where: { company: company, nextWeekLocation: EmployeeLocation.Office }, relations: ['employee', 'employee.user', 'company', 'company.floorPlanning'] });
                const nextEmployees = nextWorkSchedule.map((workSchedule) => workSchedule.employee)
                await this.deskService.arrangeDesksCurrentWeek(company.floorPlanning, currentEmployees)
                await this.deskService.arrangeDesksNextWeek(company.floorPlanning, nextEmployees)
            } else {
                const currentWorkSchedule = await this.workScheduleRepository.find({ where: { company: company, nextWeekLocation: EmployeeLocation.Office }, relations: ['employee', 'employee.user', 'company', 'company.floorPlanning'] });
                const currentEmployees = currentWorkSchedule.map((workSchedule) => workSchedule.employee)
                const nextWorkSchedule = await this.workScheduleRepository.find({ where: { company: company, currentWeekLocation: EmployeeLocation.Office }, relations: ['employee', 'employee.user', 'company', 'company.floorPlanning'] });
                const nextEmployees = nextWorkSchedule.map((workSchedule) => workSchedule.employee)
                await this.deskService.arrangeDesksCurrentWeek(company.floorPlanning, currentEmployees)
                await this.deskService.arrangeDesksNextWeek(company.floorPlanning, nextEmployees)
            }

        }
    }

    private async getWeekNumber(d): Promise<number> {
        // Copy date so don't modify original
        d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
        // Get first day of year
        const yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
        // Calculate full weeks to nearest Thursday
        const weekNo = Math.ceil((((d - +yearStart) / 86400000) + 1) / 7);
        // Return array of year and week number
        return weekNo;
    }

}
