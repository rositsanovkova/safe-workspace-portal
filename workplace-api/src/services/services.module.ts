import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/constant/secret';
import { AuthService } from './auth.service';
import { User } from 'src/models/user.entity';
import { Token } from 'src/models/token.entity';
import { Company } from 'src/models/company.entity';
import { Employee } from 'src/models/employee.entity';
import { Project } from 'src/models/projects.entity';
import { Ratio } from 'src/models/ratio.entity';
import { UsersService } from './user.service';
import { TransformService } from './transform.service';
import { JwtStrategy } from './strategy/jwt-strategy';
import { ProjectService } from './project.service';
import { EmployeeService } from './employee.service';
import { CompanyService } from './company.service';
import { CountryService } from './country.service';
import { Countries } from 'src/models/countries.entity';
import { TasksService } from './task.service';
import { FloorPlanningService } from './floor-planning.service';
import { FloorPlanning } from 'src/models/floor-planning.entity';
import { WorkStationService } from './workstation.service';
import { WorkStation } from 'src/models/workstation.entity';
import { Desk } from 'src/models/desk.entity';
import { WorkSchedule } from 'src/models/work-schedule.entity';
import { DeskService } from './desk.service';
import { CurrentEmployeeService } from './current-employee.service';
import { VacationService } from './vacation.service';
import { Vacation } from 'src/models/vacation.entity';
import { WorkScheduleService } from './work-schedule.service';
import { AdminService } from './admin.service';


@Module({
  imports: [

    TypeOrmModule.forFeature([User, Token, Company, Employee, Project, Ratio, Countries, FloorPlanning, WorkStation, Desk, WorkSchedule, Vacation, WorkSchedule]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '7d',
      }
    })
  ],
  providers:
    [
      AuthService, AdminService, UsersService, TransformService, JwtStrategy, ProjectService, EmployeeService, CompanyService, CountryService, TasksService, FloorPlanningService, WorkStationService, DeskService, CurrentEmployeeService, VacationService, WorkScheduleService
    ],
  exports:
    [
      AuthService, AdminService, UsersService, TransformService, JwtStrategy, ProjectService, EmployeeService, CompanyService, CountryService, TasksService, FloorPlanningService, WorkStationService, DeskService, CurrentEmployeeService, VacationService, WorkScheduleService
    ]
})
export class ServicesModule { }
