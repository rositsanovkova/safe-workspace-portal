import { Injectable, UnauthorizedException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { JWTPayload } from 'src/common/jwt-payload';
import * as bcrypt from 'bcrypt';
import { Token } from 'src/models/token.entity';

@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Token) private readonly tokenRepository: Repository<Token>,

        private readonly jwtService: JwtService,
    ) { }

    async findUserByName(username: string) {

        return await this.userRepository.findOne({
            where: {
                username,
                isDeleted: false,
            }
        });
    }

    async findUserByMail(mail: string) {

        return await this.userRepository.findOne({
            where: {
                mail,
                isDeleted: false,
            }
        });
    }

    async blacklist(token: string) {
        const tokenEntity = this.tokenRepository.create();
        tokenEntity.token = token;

        await this.tokenRepository.save(tokenEntity)
    }

    async isBlacklisted(token: string): Promise<boolean> {

        return Boolean(await this.tokenRepository.findOne({
            where: {
                token,
            }
        }));
    }

    async validateUserByUsername(username: string, password: string) {
        const user = await this.findUserByName(username);

        if (!user) {
            throw new NotFoundException('Such user does not exist');
        }

        const isUserValidated = await bcrypt.compare(password, user.password);

        if (!isUserValidated) {
            throw new NotFoundException('Wrong Password');
        }
        return user;

    }

    async validateUserByMail(mail: string, password: string) {
        const user = await this.findUserByMail(mail);
        if (!user) {
            throw new NotFoundException('Such user does not exist');
        }
        const isUserValidated = await bcrypt.compare(password, user.password);

        if (!isUserValidated) {
            throw new NotFoundException('Wrong Password');
        }
        return user;

    }

    async loginWithUsername(username: string, password: string): Promise<{ token: string }> {
        const user = await this.validateUserByUsername(username, password);

        if (!user) {
            throw new UnauthorizedException('Wrong credentials!');
        }

        const payload: JWTPayload = {
            id: user.id,
            username: user.username,
            mail: user.mail,
            role: user.role,
        }

        const token = await this.jwtService.signAsync(payload);

        return {
            token,
        };
    }

    async loginWithMail(mail: string, password: string): Promise<{ token: string }> {
        const user = await this.validateUserByMail(mail, password);

        if (!user) {
            throw new UnauthorizedException('Wrong credentials!');
        }

        const payload: JWTPayload = {
            id: user.id,
            mail: user.mail,
            username: user.username,
            role: user.role,
        }

        const token = await this.jwtService.signAsync(payload);

        return {
            token,
        };
    }

}
