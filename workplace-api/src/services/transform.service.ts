import { User } from 'src/models/user.entity';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';
import { Project } from 'src/models/projects.entity';
import { ReturnProjectDTO } from 'src/dtos/project/return-project.dto';
import { Employee } from 'src/models/employee.entity';
import { ReturnEmployeeDTO } from 'src/dtos/employee/return-employee.dto';
import { Company } from 'src/models/company.entity';
import { ReturnCompanyDTO } from 'src/dtos/company/return-company.dto';
import { ReturnFloorPlanningDTO } from 'src/dtos/floor-planning/return-floor-planning.dto';
import { FloorPlanning } from 'src/models/floor-planning.entity';
import { WorkStation } from 'src/models/workstation.entity';
import { ReturnWorkstationDTO } from 'src/dtos/workstation/return-workstation.dto';
import { Desk } from 'src/models/desk.entity';
import { ReturnDeskDTO } from 'src/dtos/desk/return-desk.dto';
import { WorkSchedule } from 'src/models/work-schedule.entity';
import { Vacation } from 'src/models/vacation.entity';
import { ReturnVacationDTO } from 'src/dtos/vacation/return-vacation.dto';
import { ReturnWorkingScheduleDTO } from 'src/dtos/currentEmployee/return-working-schedule.dto';

export class TransformService {
  toReturnUserDTO(user: User): ReturnUserDTO {
    return {
      id: user.id,
      username: user.username,
      country: user.country,
      mail: user.mail,
      fullName: user.fullName,
      employee: user.employee,
      vacation: user.vacation,
      role: user.role,
    }
  }
  toReturnProjectDTO(project: Project): ReturnProjectDTO {
    return {
      id: project.id,
      title: project.title,
      employees: project.employees ? project.employees.map((employee) => this.toReturnEmployeeDTO(employee)) : undefined,
      company: project.company ? this.toReturnCompanyDTO(project.company) : undefined,
    }
  }
  toReturnEmployeeDTO(employee: Employee): ReturnEmployeeDTO {
    return {
      id: employee.id,
      user: employee.user ? this.toReturnUserDTO(employee.user) : undefined,
      project: employee.project ? this.toReturnProjectDTO(employee.project) : undefined,
      company: this.toReturnCompanyDTO(employee.company),
      desk: employee.desk ? this.toReturnDeskDTO(employee.desk) : undefined,
      workSchedule: employee.workSchedule ? this.toReturnWorkScheduleDTO(employee.workSchedule) : undefined,
    }
  }
  toReturnCompanyDTO(company: Company): ReturnCompanyDTO {
    return {
      id: company.id,
      country: company.country,
      projects: company.projects ? company.projects.map(project => this.toReturnProjectDTO(project)) : undefined,
      floorPlanning: company.floorPlanning ? this.toReturnFloorPlanningDTO(company.floorPlanning) : undefined,
      employees: company.employees ? company.employees.map(employee => this.toReturnEmployeeDTO(employee)) : undefined,
      ratio: company.ratio,
    }
  }

  toReturnFloorPlanningDTO(floorPlanning: FloorPlanning): ReturnFloorPlanningDTO {
    return {
      id: floorPlanning.id,
      numberOfDesks: floorPlanning.numberOfDesks,
      workStationColumns: floorPlanning.workStationColumns,
      workStation: floorPlanning.workStation ? floorPlanning.workStation.map(workstation => this.toReturnWorkStationDTO(workstation)) : undefined,
      company: floorPlanning.company ? this.toReturnCompanyDTO(floorPlanning.company) : undefined,
    }
  }

  toReturnWorkStationDTO(workStation: WorkStation): ReturnWorkstationDTO {
    return {
      id: workStation.id,
      rowDesks: workStation.rowDesks,
      columnDesks: workStation.columnDesks,
      desks: workStation.desks ? workStation.desks.map((desk) => this.toReturnDeskDTO(desk)) : undefined,
      floorPlanning: workStation.floorPlanning ? this.toReturnFloorPlanningDTO(workStation.floorPlanning) : undefined,
    }
  }
  toReturnDeskDTO(desk: Desk): ReturnDeskDTO {
    return {
      id: desk.id,
      employee: desk.employee ? this.toReturnEmployeeDTO(desk.employee) : undefined,
      status: desk.status,
      workstation: desk.workstation ? this.toReturnWorkStationDTO(desk.workstation) : undefined,
      statusNextWeek: desk.statusNextWeek,
      employeeNextWeek: desk.employeeNextWeek ? this.toReturnEmployeeDTO(desk.employeeNextWeek) : undefined,
    }
  }

  toReturnWorkScheduleDTO(workSchedule: WorkSchedule): ReturnWorkingScheduleDTO {
    return {
      id: workSchedule.id,
      employee: workSchedule.employee ? this.toReturnEmployeeDTO(workSchedule.employee) : undefined,
      project: workSchedule.project ? this.toReturnProjectDTO(workSchedule.project) : undefined,
      company: workSchedule.company ? this.toReturnCompanyDTO(workSchedule.company) : undefined,
      currentWeekLocation: workSchedule.currentWeekLocation,
      nextWeekLocation: workSchedule.nextWeekLocation,
    }
  }

  toReturnVacationDTO(vacation: Vacation): ReturnVacationDTO {
    return {
      id: vacation.id,
      startDate: vacation.startDate,
      endDate: vacation.endDate,
      user: vacation.user ? this.toReturnUserDTO(vacation.user) : undefined,
      status: vacation.status,
    }
  }
}
