import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { Company } from 'src/models/company.entity';
import { FloorPlanning } from 'src/models/floor-planning.entity';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { ReturnWorkstationDTO } from 'src/dtos/workstation/return-workstation.dto';
import { WorkStation } from 'src/models/workstation.entity';
import { CreateWorkstationDTO } from 'src/dtos/workstation/create-workstation.dto';
import { Desk } from 'src/models/desk.entity';
import { ReturnDeskDTO } from 'src/dtos/desk/return-desk.dto';
import { DeskService } from './desk.service';

@Injectable()
export class WorkStationService {
    constructor(
        private readonly transformer: TransformService,
        private readonly deskService: DeskService,
        @InjectRepository(Company) private readonly companyRepository: Repository<Company>,
        @InjectRepository(WorkStation) private readonly workStationRepository: Repository<WorkStation>,
        @InjectRepository(FloorPlanning) private readonly floorPlanningRepository: Repository<FloorPlanning>,
        @InjectRepository(Desk) private readonly deskRepository: Repository<Desk>,
    ) { }

    async getAll(): Promise<ReturnWorkstationDTO[]> {
        const workStations = await this.workStationRepository.find({ relations: ['desks'] });
        if (!workStations || workStations.length === 0) {
            throw new NotFoundException('No companies are found')
        }

        return workStations.map(workStation => this.transformer.toReturnWorkStationDTO(workStation));
    }

    async getById(floorPlanningId: number): Promise<ReturnWorkstationDTO> {
        const foundFloorPlanning = await this.companyRepository.findOne({ where: { id: floorPlanningId } })
        const workStation = await this.workStationRepository.findOne({ where: { floorPlanning: foundFloorPlanning }, relations: ['desks', 'desks.workstation', 'floorPlanning'] });
        if (!workStation) {
            throw new NotFoundException('Such floor planning does not exist')
        }

        return this.transformer.toReturnWorkStationDTO(workStation);
    }

    async create(workStationDTO: CreateWorkstationDTO): Promise<ReturnMessageDTO> {
        const companyId = +workStationDTO.floorPlanning;
        console.log(companyId)
        const foundCompany = await this.companyRepository.findOne({ where: { id: companyId } })
        console.log(foundCompany)
        const foundFloorPlanning = await this.floorPlanningRepository.findOne({ where: { company: foundCompany.id }, relations: ['company'] });
        const checkWorkStations = await this.workStationRepository.find({ where: { floorPlanning: foundFloorPlanning.id }, relations: ['desks'] })
        const number = checkWorkStations.reduce((acc, workstation) => acc += workstation.desks.length, 0);

        const totalDesks = foundFloorPlanning.numberOfDesks;
        console.log(checkWorkStations)
        console.log(totalDesks)
        
        const availableDesks = totalDesks - number - workStationDTO.rowDesks * workStationDTO.columnDesks;
        console.log(availableDesks)
        if (availableDesks < 0) {
            throw new BadRequestException(`${totalDesks - number} desks available to place`);
        }
        const createdWorkStation = this.workStationRepository.create();
        createdWorkStation.rowDesks = workStationDTO.rowDesks;
        createdWorkStation.columnDesks = workStationDTO.columnDesks;
        createdWorkStation.floorPlanning = foundFloorPlanning;

        const numberOfDesks = workStationDTO.rowDesks * workStationDTO.columnDesks;
        await this.workStationRepository.save(createdWorkStation);
        let desk = new Desk();
        const arrayDesks = [];
        for (let i = 0; i < numberOfDesks; i++) {
            arrayDesks.push(i);
        }
        await this.workStationRepository.save(createdWorkStation);
        
        const forEachArrayDesks = async () => {
            arrayDesks.forEach(( () => {
                desk = this.deskRepository.create();
                return this.deskRepository.save(desk);
            }))

            return await Promise.all(arrayDesks)
        }
        await forEachArrayDesks()

        await this.workStationRepository.save(createdWorkStation);
        const foundDesks = await this.deskRepository.find({ where: { workstation: createdWorkStation } });
        createdWorkStation.desks = foundDesks;
        await this.workStationRepository.save(createdWorkStation);
        await this.floorPlanningRepository.update(foundFloorPlanning.id, foundFloorPlanning);
        await this.assignDesks(createdWorkStation.id);
        return { message: 'Floor planning created' };
    }

    async assignDesks(workStationID: number): Promise<ReturnDeskDTO[]> {
        const foundWorkStation = await this.workStationRepository.findOne({ where: { id: workStationID }, relations: ['floorPlanning', 'floorPlanning.workStation', 'floorPlanning.workStation.desks', 'floorPlanning.company'] });


        const foundDesks = await this.deskRepository.find({ where: { workstation: null } });

        const mappedDesks = async () => {
            const availableDesks = foundDesks.map(async desk => {
                desk.workstation = foundWorkStation;
                await this.deskRepository.update(desk.id, desk);
                return desk;
            });

            return await Promise.all(availableDesks);
        }

        const newDesks = await mappedDesks();
        await this.deskService.arrangeDesksCurrentWeek(foundWorkStation.floorPlanning, [])

        return newDesks.map(desk => this.transformer.toReturnDeskDTO(desk));
    }
}
