import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { AuthService } from '../auth.service';
import { jwtConstants } from 'src/constant/secret';
import { JWTPayload } from 'src/common/jwt-payload';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: JWTPayload) {

    const userByUsername = await this.authService.findUserByName(payload.username);
    const userByMail = await this.authService.findUserByMail(payload.username);
    if (!userByUsername && !userByMail) {

      return;
    }

    if (!userByUsername) {

      return userByMail;
    }

    if (!userByMail) {

      return userByUsername;
    }
  }
}
