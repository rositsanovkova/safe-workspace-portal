import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { Employee } from 'src/models/employee.entity';
import { User } from 'src/models/user.entity';
import { Company } from 'src/models/company.entity';
import { WorkSchedule } from 'src/models/work-schedule.entity';
import { ReturnWorkingScheduleDTO } from 'src/dtos/currentEmployee/return-working-schedule.dto';

@Injectable()
export class CurrentEmployeeService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Employee) private readonly employeeRepository: Repository<Employee>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Company) private readonly companyRepository: Repository<Company>,
        @InjectRepository(WorkSchedule) private readonly workScheduleRepository: Repository<WorkSchedule>,

    ) { }

    async getByCompany(companyId: number): Promise<ReturnWorkingScheduleDTO[]> {
        const company = await this.companyRepository.findOne({ where: { id: companyId } })
        const currentEmployees = await this.workScheduleRepository.find(
            {
                where: {
                    company: company,
                },
                relations: ['user', 'company', 'project']
            });

        if (!currentEmployees) {
            throw new BadRequestException('No such employee');
        }

        return currentEmployees.map(currentEmployee => this.transformer.toReturnWorkScheduleDTO(currentEmployee));
    }
}
