import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { Company } from 'src/models/company.entity';
import { ReturnCompanyDTO } from 'src/dtos/company/return-company.dto';
import { Ratio } from 'src/models/ratio.entity';


@Injectable()
export class CompanyService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Company) private readonly companyRepository: Repository<Company>,
        @InjectRepository(Ratio) private readonly ratioRepository: Repository<Ratio>,
    ) { }

    async getAll(): Promise<ReturnCompanyDTO[]> {
        const companies = await this.companyRepository.find({
            relations: [
                'projects',
                'floorPlanning',
                'employees',
                'employees.project',
                'employees.desk',
                'employees.user',
                'employees.company',
                'projects.company',
                'projects.employees',
                'projects.employees.user',
                'projects.employees.company',
            ]
        });
        if (!companies || companies.length === 0) {
            throw new NotFoundException('No companies are found')
        }
        return companies.map(company => this.transformer.toReturnCompanyDTO(company));
    }

    async getByID(companyID: number): Promise<ReturnCompanyDTO> {
        const company = await this.companyRepository.findOne({
            where: { id: companyID },
            relations: [
                'projects',
                'floorPlanning',
                'employees',
                'employees.company',
                'projects.employees',
                'projects.employees.company',
                'employees.user',
                'projects.employees.user',
                'floorPlanning.workStation',
                'floorPlanning.workStation.desks'
            ]
        });
        if (!company) {
            throw new NotFoundException('No such company exists')
        }
        return this.transformer.toReturnCompanyDTO(company);
    }

}
