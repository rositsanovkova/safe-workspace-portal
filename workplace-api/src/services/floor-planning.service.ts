import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { Company } from 'src/models/company.entity';
import { FloorPlanning } from 'src/models/floor-planning.entity';
import { ReturnFloorPlanningDTO } from 'src/dtos/floor-planning/return-floor-planning.dto';
import { CreateFloorPlanningDTO } from 'src/dtos/floor-planning/create-floor-planning.dto';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { ReturnCompanyDTO } from 'src/dtos/company/return-company.dto';

@Injectable()
export class FloorPlanningService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Company) private readonly companyRepository: Repository<Company>,
        @InjectRepository(FloorPlanning) private readonly floorPlanningRepository: Repository<FloorPlanning>,
    ) { }

    async getAll(): Promise<ReturnFloorPlanningDTO[]> {
        const floorPlannings = await this.floorPlanningRepository.find();
        if (!floorPlannings || floorPlannings.length === 0) {
            throw new NotFoundException('No companies are found')
        }

        return floorPlannings.map(floorPlanning => this.transformer.toReturnFloorPlanningDTO(floorPlanning));
    }

    async getById(companyId: number): Promise<ReturnFloorPlanningDTO> {
        const foundCompany = await this.companyRepository.findOne({ where: { id: companyId } })
        const floorPlanning = await this.floorPlanningRepository.findOne({
            where: { company: foundCompany }, relations:
                ['company',
                    'workStation',
                    'workStation.desks',
                    'workStation.desks.employee',
                    'workStation.desks.employee.company',
                    'workStation.desks.employee.user',
                    'workStation.desks.employeeNextWeek',
                    'workStation.desks.employeeNextWeek.user',
                    'workStation.desks.employeeNextWeek.company'
                ]
        });

        if (!floorPlanning) {
            throw new NotFoundException('Such floor planning does not exist')
        }

        return this.transformer.toReturnFloorPlanningDTO(floorPlanning);
    }

    async create(floorPlanningDto: CreateFloorPlanningDTO): Promise<ReturnMessageDTO> {
        const foundCompany = await this.companyRepository.findOne({ where: { country: floorPlanningDto.company } });

        const checkFloorPlanning = await this.floorPlanningRepository.findOne({ where: { company: foundCompany }, relations: ['company'] })
        if (checkFloorPlanning) {
            throw new BadRequestException('Such floor chart already exists')
        }

        const createdFloorPlanning = this.floorPlanningRepository.create();
        createdFloorPlanning.numberOfDesks = floorPlanningDto.numberOfDesks;
        createdFloorPlanning.workStationColumns = floorPlanningDto.workStationColumns;

        createdFloorPlanning.company = foundCompany;
        this.floorPlanningRepository.save(createdFloorPlanning);

        return { message: 'Floor planning created' };
    }

    async getAllCompaniesWithoutFloorPlanning(): Promise<ReturnCompanyDTO[]> {
        const companies = await this.companyRepository.find({
            relations: [
                'projects',
                'employees',
                'floorPlanning',
                'employees.project',
                'employees.desk',
                'employees.user',
                'employees.company',
                'projects.company',
                'projects.employees',
                'projects.employees.user',
                'projects.employees.company',
            ],
        });

        if (!companies || companies.length === 0) {
            throw new NotFoundException('No companies are found')
        }

        return companies.map(company => this.transformer.toReturnCompanyDTO(company));
    }
}
