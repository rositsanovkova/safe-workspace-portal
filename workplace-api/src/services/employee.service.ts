import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { Employee } from 'src/models/employee.entity';
import { ReturnEmployeeDTO } from 'src/dtos/employee/return-employee.dto';
import { User } from 'src/models/user.entity';
import { Company } from 'src/models/company.entity';

@Injectable()
export class EmployeeService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Employee) private readonly employeeRepository: Repository<Employee>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Company) private readonly companyRepository: Repository<Company>,


    ) { }

    async getAll(): Promise<ReturnEmployeeDTO[]> {
        const employees = await this.employeeRepository.find({
            where: {
                isDeleted: false,
            },
            relations: ['user', 'project', 'company', 'desk', 'workSchedule']
        });

        if (!employees || employees.length === 0) {
            throw new NotFoundException('No employees are found')
        }

        return employees.map(employee => this.transformer.toReturnEmployeeDTO(employee));
    }

    async getAllAvailable(countryId: number): Promise<ReturnEmployeeDTO[]> {

        const foundCompany = await this.companyRepository.findOne({ where: { id: countryId } })

        const foundEmployees = await this.employeeRepository.find({ where: { project: null, company: foundCompany }, relations: ['user', 'company', 'workSchedule'] });

        return foundEmployees.map(employee => this.transformer.toReturnEmployeeDTO(employee));
    }

    async getById(employeeId: number): Promise<ReturnEmployeeDTO> {

        const employee = await this.employeeRepository.findOne(
            {
                where: {
                    id: employeeId,
                    isDeleted: false,
                },
                relations: ['workSchedule']
            });

        if (!employee) {
            throw new BadRequestException('No such employee');
        }

        return this.transformer.toReturnEmployeeDTO(employee);
    }
}
