import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { Company } from 'src/models/company.entity';
import { Employee } from 'src/models/employee.entity';
import { WorkSchedule } from 'src/models/work-schedule.entity';
import { ReturnFloorPlanningDTO } from 'src/dtos/floor-planning/return-floor-planning.dto';
import { ReturnEmployeeDTO } from 'src/dtos/employee/return-employee.dto';
import { Desk } from 'src/models/desk.entity';
import { DeskStatus } from 'src/enums/desk-status';
import { ReturnDeskDTO } from 'src/dtos/desk/return-desk.dto';


@Injectable()
export class DeskService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Employee) private readonly employeeRepository: Repository<Employee>,
        @InjectRepository(Company) private readonly companyRepository: Repository<Company>,
        @InjectRepository(WorkSchedule) private readonly workScheduleRepository: Repository<WorkSchedule>,
        @InjectRepository(Desk) private readonly deskRepository: Repository<Desk>,
    ) { }

    async arrangeDesksCurrentWeek(floorPlanningDTO: ReturnFloorPlanningDTO, currentWeekEmployees: ReturnEmployeeDTO[]): Promise<void> {
        const mappedWorkStations = async () => {
            const mapped = floorPlanningDTO.workStation.map(async (workstation) => await this.deskRepository.find({ where: { workstation: workstation }, relations: ['employee'] }))

            return await Promise.all(mapped);
        }

        const foundDesks = await mappedWorkStations();
        const desks: Desk[] = [].concat(...foundDesks)
        const deskArray = [];
        let change = false;
        let count = 0;
        let countEmployees = 0
        const updateName = (name: ReturnEmployeeDTO) => {
            deskArray.push(name);
            countEmployees++;
        }
        if (floorPlanningDTO.company.ratio >= 0.05) {
            floorPlanningDTO.workStation.forEach((workStation) => {
                for (let row = 1; row <= workStation.rowDesks; row++) {

                    for (let col = 1; col <= workStation.columnDesks; col++) {
                        if (workStation.columnDesks % 2 === 0) {
                            change = row % 2 === 0;
                        }
                        change
                            ? count % 2 === 0
                                ? deskArray.push("forbidden")
                                : currentWeekEmployees && countEmployees < currentWeekEmployees.length
                                    ? updateName(currentWeekEmployees[countEmployees])
                                    : deskArray.push("available")
                            : count % 2 === 0
                                ? currentWeekEmployees && countEmployees < currentWeekEmployees.length
                                    ? updateName(currentWeekEmployees[countEmployees])
                                    : deskArray.push("available")
                                : deskArray.push("forbidden")
                        count++
                    }
                }
                count = 0;
            change = false
            })
            
        }
        if (floorPlanningDTO.company.ratio < 0.05) {
            floorPlanningDTO.workStation.forEach((workStation) => {
                for (let row = 1; row <= workStation.rowDesks; row++) {

                    for (let col = 1; col <= workStation.columnDesks; col++) {
                        currentWeekEmployees && countEmployees < currentWeekEmployees.length
                            ? updateName(currentWeekEmployees[countEmployees])
                            : deskArray.push("available")
                    }
                }
                count = 0;
        change = false
            })
            
        }
        

        await this.mappedDesksCurrentWeek(desks, deskArray);
    }

    async arrangeDesksNextWeek(floorPlanningDTO: ReturnFloorPlanningDTO, nextWeekEmployees: ReturnEmployeeDTO[]): Promise<void> {
        const mappedWorkStations = async () => {
            const mapped = floorPlanningDTO.workStation.map(async (workstation) => await this.deskRepository.find({ where: { workstation: workstation }, relations: ['employee'] }))
            return await Promise.all(mapped);
        }
        const foundDesks = await mappedWorkStations();
        const desks: Desk[] = [].concat(...foundDesks)
        const deskArray = [];
        let change = false;
        let count = 0;
        let countEmployees = 0
        const updateName = (name: ReturnEmployeeDTO) => {
            deskArray.push(name);
            countEmployees++;
        }
        if (floorPlanningDTO.company.ratio >= 0.05) {
            floorPlanningDTO.workStation.forEach((workStation) => {
                console.log(count)
                for (let row = 1; row <= workStation.rowDesks; row++) {

                    for (let col = 1; col <= workStation.columnDesks; col++) {
                        if (workStation.columnDesks % 2 === 0) {
                            change = row % 2 === 0;
                        }
                        change
                            ? count % 2 === 0
                                ? deskArray.push("forbidden")
                                : nextWeekEmployees && countEmployees < nextWeekEmployees.length
                                    ? updateName(nextWeekEmployees[countEmployees])
                                    : deskArray.push("available")
                            : count % 2 === 0
                                ? nextWeekEmployees && countEmployees < nextWeekEmployees.length
                                    ? updateName(nextWeekEmployees[countEmployees])
                                    : deskArray.push("available")
                                : deskArray.push("forbidden")
                        count++
                    }
                }
                count = 0;
                change = false
            })
            
        }
        if (floorPlanningDTO.company.ratio < 0.05) {
            floorPlanningDTO.workStation.forEach((workStation) => {
                for (let row = 1; row <= workStation.rowDesks; row++) {

                    for (let col = 1; col <= workStation.columnDesks; col++) {
                        nextWeekEmployees && countEmployees < nextWeekEmployees.length
                            ? updateName(nextWeekEmployees[countEmployees])
                            : deskArray.push("available")
                    }
                }
                count = 0;
                change = false
            })
            
        }


        await this.mappedDesksNextWeek(desks, deskArray);
    }

    async mappedDesksCurrentWeek(desks: Desk[], deskArray): Promise<ReturnDeskDTO[]> {

        const mapped = async () => {
            const map = desks.map(async (desk, index) => {
                desk.status = DeskStatus.Available;
                desk.employee = null;
                if (deskArray[index] === 'available') {
                    desk.status = DeskStatus.Available;
                    desk.employee = null;
                }
                if (deskArray[index] === 'forbidden') {
                    desk.status = DeskStatus.Forbidden;
                    desk.employee = null
                }
                if (deskArray[index] !== 'available' && deskArray[index] !== 'forbidden') {
                    desk.status = DeskStatus.Taken;
                    desk.employee = deskArray[index];
                }
                await this.deskRepository.update(desk.id, desk)
                return desk;
            })
            return await Promise.all(map)
        }
        const assignedDesks = await mapped();
        return assignedDesks;
    }

    async mappedDesksNextWeek(desks: Desk[], deskArray): Promise<ReturnDeskDTO[]> {
        const mapped = async () => {
            const map = desks.map(async (desk, index) => {
                desk.statusNextWeek = DeskStatus.Available;
                desk.employeeNextWeek = null;
                if (deskArray[index] === 'available') {
                    desk.statusNextWeek = DeskStatus.Available;
                    desk.employeeNextWeek = null;
                }
                if (deskArray[index] === 'forbidden') {
                    desk.statusNextWeek = DeskStatus.Forbidden;
                    desk.employeeNextWeek = null
                }
                if (deskArray[index] !== 'available' && deskArray[index] !== 'forbidden') {
                    desk.statusNextWeek = DeskStatus.Taken;
                    desk.employeeNextWeek = deskArray[index];
                }
                await this.deskRepository.update(desk.id, desk)
                return desk;
            })
            return await Promise.all(map)
        }
        const assignedDesks = await mapped();
        return assignedDesks;
    }

}
