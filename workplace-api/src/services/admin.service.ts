import { User } from 'src/models/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { BadRequestException } from '@nestjs/common/exceptions/bad-request.exception';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';

@Injectable()
export class AdminService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
    ) { }

    async changeRole(user, userId): Promise<ReturnUserDTO> {

        const foundUser = await this.usersRepository.findOne(userId);
        if (foundUser.role === user.role) {
            throw new BadRequestException('This user already has this role')
        }
        await this.usersRepository.update(userId, user);

        return await this.usersRepository.findOne(userId);
    }
    
}