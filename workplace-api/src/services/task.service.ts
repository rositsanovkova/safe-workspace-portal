import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { TransformService } from './transform.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Company } from 'src/models/company.entity';
import { Repository } from 'typeorm';
import { Ratio } from 'src/models/ratio.entity';
import fetch from 'node-fetch'
import * as sgMail from '@sendgrid/mail';
import { User } from 'src/models/user.entity';
import { WorkScheduleService } from './work-schedule.service';
import { WorkSchedule } from 'src/models/work-schedule.entity';
import { Desk } from 'src/models/desk.entity';
import { DeskStatus } from 'src/enums/desk-status';


@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(
    private readonly transformer: TransformService,
    private readonly workSchedule: WorkScheduleService,
    @InjectRepository(Company) private readonly companyRepository: Repository<Company>,
    @InjectRepository(Ratio) private readonly ratioRepository: Repository<Ratio>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(WorkSchedule) private readonly workScheduleRepository: Repository<WorkSchedule>,
    @InjectRepository(Desk) private readonly deskRepository: Repository<Desk>,
  ) { }


  @Cron('0 04 01 * * *')
  async getCountryData() {
    const companies = await this.companyRepository.find();
    const countryNames = companies.map(company => company.country);
    countryNames.map(countryName => {
      const createRatio = this.ratioRepository.create()
      fetch(`https://coronavirus-19-api.herokuapp.com/countries/${countryName}`)
        .then(function (res) {
          return res.json();
        })
        .then(async (result) => {
          createRatio.country = result.country;
          createRatio.millTests = result.testsPerOneMillion;
          createRatio.dailyCases = result.todayCases;
          this.ratioRepository.save(createRatio);
        })
        .catch();
    })
  }

  @Cron('0 40 10 * * *')
  async calculateRatio() {
    const ratios = await this.ratioRepository.find();
    const ratioByCountry = async (country: string) => {
      const ratiosByCountry = await this.ratioRepository.find({ where: { country: country } });

      if (ratiosByCountry.length >= 7) {
        ratiosByCountry.slice(ratiosByCountry.length - 7, ratiosByCountry.length);
      }

      const last7DaysDailyCases = ratiosByCountry.reduce((acc, ratio) => acc += ratio.dailyCases, 0);
      const averageNumber = ratiosByCountry.length;
      const millTestByCountry = ratiosByCountry.pop();
      const ratio = (last7DaysDailyCases / averageNumber) / millTestByCountry.millTests
      const foundCompany = await this.companyRepository.findOne({ where: { country: country } });
      foundCompany.ratio = +ratio.toFixed(2);
      await this.companyRepository.save(foundCompany)
      console.log(foundCompany)
      return ratio;
    }
    ratios.map(ratio => {
      ratioByCountry(ratio.country)
    })

  }

  @Cron('0 25 13 * * 5')
  async sendNotifications() {
    const users = await this.userRepository.find();
    const recepients = users.map((user) => {
      return user.mail;
    })
    console.log(recepients)
    // const recepients = ['fgsgsgfsgs@gmail.com', 'r.novkova@gmail.com']
    sgMail.setApiKey('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');

    recepients.map((recepient) => {
      const msg = {
        to: recepient,
        from: 'safeworkspaceportal@gmail.com',
        subject: 'Check your new weekly Schedule',
        text: 'Log to the system and check what will be your work location next week!',
        html: '<strong>Log to the system and check what will be your work location next week!</strong>',
      };
      sgMail.send(msg).then(() => {
        console.log('Message sent')
      }).catch((error) => {
        console.log(error.response.body)
        // console.log(error.response.body.errors[0].message)
      })
    })

  }

  @Cron('0 35 12 * * 5')
  async testNotifications() {

    sgMail.setApiKey('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');

    const msg = {
      to: 'safeworkspaceportal@gmail.com',
      from: 'safeworkspaceportal@gmail.com',
      subject: 'The Schedule for next week is ready!',
      text: 'Log to the system and check what will be your work location next week!',
      html: '<strong>Log to the system and check what will be your work location next week!</strong>',
    };
    sgMail.send(msg).then(() => {
      console.log('Message sent')
    }).catch((error) => {
      console.log(error.response.body)
      // console.log(error.response.body.errors[0].message)
    })
  }

  @Cron('0 40 13 * * 5')
  async updateWorkSchedule() {
    const companies = await this.companyRepository.find({ relations: ['floorPlanning', 'floorPlanning.workStation', 'floorPlanning.company', 'floorPlanning.workStation.desks'] });
    // const workSchedules = await this.workScheduleRepository.find({relations: ['employee']})
    const desks = await this.deskRepository.find();
    const clearDesks = async () => {
      const clearedDesks = desks.map((desk) => {
        desk.employee = null;
        desk.employeeNextWeek = null;
        desk.status = DeskStatus.Available;
        desk.statusNextWeek = DeskStatus.Available;
        this.deskRepository.update(desk.id, desk);

      })
      return await Promise.all(clearedDesks)
    }

    await clearDesks();
    const mapping = async () => {
      const mappedSchedules = companies.map((company) => {
        this.workSchedule.employeeSchedule(company)
      })

      return await Promise.all(mappedSchedules)
    }
    await mapping()
    console.log('updated')
  }

}
