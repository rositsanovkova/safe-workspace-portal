import { User } from 'src/models/user.entity';
import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDTO } from 'src/dtos/users/create-user.dto';
import { UpdateUserDTO } from 'src/dtos/users/update-user.dto';
import * as bcrypt from 'bcrypt';
import { UserRole } from 'src/enums/user-role';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';
import { TransformService } from './transform.service';
import { Company } from 'src/models/company.entity';
import { Employee } from 'src/models/employee.entity';

@Injectable()
export class UsersService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Company) private readonly companyRepository: Repository<Company>,
        @InjectRepository(Employee) private readonly employeeRepository: Repository<Employee>,

    ) { }

    async getById(userId: number): Promise<ReturnUserDTO> {
        const user = await this.usersRepository.findOne(
            {
                where: {
                    id: userId,
                    isDeleted: false,
                },
                relations: ['employee', 'employee.project', 'employee.desk', 'vacation']
            });

        if (!user) {
            throw new BadRequestException('No such user');
        }

        return this.transformer.toReturnUserDTO(user);
    }

    async create(userDTO: CreateUserDTO): Promise<ReturnMessageDTO> {

        const checkUsers = await this.usersRepository.find({ where: { username: userDTO.username } });
        if (checkUsers.length > 0) {
            throw new BadRequestException('User with such username already exists')
        }

        const user = this.usersRepository.create(userDTO);

        const checkCompany = await this.companyRepository.findOne({ where: { country: userDTO.country } });
        if (!checkCompany) {
            const company = this.companyRepository.create();
            company.country = userDTO.country;
            await this.companyRepository.save(company);
        }

        user.password = await bcrypt.hash(user.password, 10);
        await this.usersRepository.save(user);
        const foundCompany = await this.companyRepository.findOne({ where: { country: userDTO.country } })
        const createdEmployee = this.employeeRepository.create();
        createdEmployee.company = foundCompany;
        createdEmployee.user = user;
        await this.employeeRepository.save(createdEmployee);

        return { message: 'User created' };
    }

    async update(userDTO: Partial<UpdateUserDTO>, loggedUser: number, userId: number): Promise<ReturnMessageDTO> {
        const user = await this.usersRepository.findOneOrFail(userId, { relations: ['employee'] });
        if (!user) {
            throw new NotFoundException('no such user')
        }

        const foundLoggedUserRole = (await this.usersRepository.findOneOrFail(loggedUser, { relations: ['employee'] })).role;
        if (loggedUser === userId || foundLoggedUserRole === UserRole.Admin) {
            user.fullName = userDTO.fullName;
            user.mail = userDTO.mail;
            user.country = userDTO.country;
            await this.usersRepository.update(userId, user);
        } else {
            throw new BadRequestException('User can\'t be update by the requesting user');
        }
        
        return { message: 'User updated' };
    }

}
