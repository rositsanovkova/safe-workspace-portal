import { User } from 'src/models/user.entity';
import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import { CreateProjectDTO } from '../dtos/project/create-project.dto'
import { Project } from 'src/models/projects.entity';
import { ReturnProjectDTO } from 'src/dtos/project/return-project.dto';
import { Company } from 'src/models/company.entity';
import { UpdateCompanyDTO } from 'src/dtos/company/update-company.dto';
import { AssignEmployeeDTO } from 'src/dtos/employee/assign-employee.dto';
import { Employee } from 'src/models/employee.entity';
import { WorkScheduleService } from './work-schedule.service';
import { WorkSchedule } from 'src/models/work-schedule.entity';

@Injectable()
export class ProjectService {
    constructor(
        private readonly transform: TransformService,
        private readonly workSchedule: WorkScheduleService,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Project) private readonly projectRepository: Repository<Project>,
        @InjectRepository(Company) private readonly companyRepository: Repository<Company>,
        @InjectRepository(Employee) private readonly employeeRepository: Repository<Employee>,
        @InjectRepository(WorkSchedule) private readonly workScheduleRepository: Repository<WorkSchedule>,
    ) { }

    async getAll(): Promise<ReturnProjectDTO[]> {
        const projects = await this.projectRepository.find({ relations: ['employees', 'company', 'employees.company', 'employees.user'] });

        return projects.map(project => this.transform.toReturnProjectDTO(project))
    }

    async getById(projectId: number): Promise<ReturnProjectDTO> {
        const project = await this.projectRepository.findOne({
            where: { id: projectId },
            relations: ['company', 'employees', 'employees.company', 'employees.user']
        });

        if (!project) {
            throw new NotFoundException('No such project exists')
        }

        return this.transform.toReturnProjectDTO(project);
    }

    async createProject(projectDTO: CreateProjectDTO, companyId: number): Promise<ReturnProjectDTO> {

        const checkProject = await this.projectRepository.find({ where: { title: projectDTO.title } });
        if (checkProject.length > 0) {
            throw new BadRequestException('Project with such title already exists')
        }

        const foundCompany = await this.companyRepository.findOne({ where: { id: companyId }, relations: ['employees', 'projects'] });

        if (!foundCompany) {
            throw new BadRequestException('Company with this name does not exist!');
        }

        const mappedEmployees = async () => {
            const foundEmployees = projectDTO.employees.map(async employee => {
                const foundUser = await this.usersRepository.findOne({ where: { fullName: employee } });
                const availableEmployees = await this.employeeRepository.findOne({ where: { user: foundUser } });
                return availableEmployees;
            })

            return await Promise.all(foundEmployees);
        }

        const newEmployees = await mappedEmployees();

        const freeEmployees = [...newEmployees];

        const project = this.projectRepository.create();
        project.title = projectDTO.title;
        project.company = foundCompany;
        project.employees = freeEmployees
        await this.projectRepository.save(project);


        if (freeEmployees.length >= 0) {
            const mappedFreeEmployees = async () => {
                const workSchedules = freeEmployees.map(async employee => {
                    const workScheduleFound = await this.workScheduleRepository.findOne({ where: { employee: employee }, relations: ['project'] })
                    if (workScheduleFound) {
                        workScheduleFound.project = project;
                        this.workScheduleRepository.save(workScheduleFound)
                    }

                    employee.project = project;
                    employee.company = foundCompany;
                    await this.employeeRepository.save(employee);
                    return workScheduleFound;
                })

                return await Promise.all(workSchedules);
            }
            await mappedFreeEmployees()
        }

        await this.projectRepository.save(project);
        const createdProject = await this.projectRepository.findOne({
            where: { title: projectDTO.title }, relations: [
                'company',
                'employees',
                'employees.company'
            ]
        });
        await this.workSchedule.createWorkSchedule(createdProject.employees.map((employee) => this.transform.toReturnEmployeeDTO(employee)))

        return this.transform.toReturnProjectDTO(createdProject);
    }

    async updateCountry(country: UpdateCompanyDTO, projectID: number): Promise<ReturnProjectDTO> {
        const company = await this.companyRepository.findOne({ where: { country: country.country } });
        const project = await this.projectRepository.findOne(projectID, { relations: ['employees'] });
        const workSchedules = await this.workScheduleRepository.find({where: {project: project}})
        
        if (!company) {
            throw new BadRequestException('Such company doesn\'t exist')
        }
        
        project.employees.map(async employee => {
            const updatedEmployee = await this.employeeRepository.findOne(employee);
            updatedEmployee.company = company;
            await this.employeeRepository.save(updatedEmployee)
        })
        workSchedules.map(async workschedule => {
            workschedule.company = company;
            await this.workScheduleRepository.update(workschedule.id, workschedule)
        })

        project.company = company;
        await this.projectRepository.save(project);
        await this.companyRepository.save(company);
        const updateProject = await this.projectRepository.findOne(projectID);

        return this.transform.toReturnProjectDTO(updateProject);
    }
    async assignEmployee(assignEmployee: AssignEmployeeDTO, projectID: number): Promise<ReturnProjectDTO> {
        const user = await this.usersRepository.findOne({ where: { fullName: assignEmployee.fullName } });
        const project = await this.projectRepository.findOne(projectID, { relations: ['company', 'employees', 'employees.user', 'employees.company', 'employees.workSchedule'] });
        const foundEmployee = await this.employeeRepository.findOne({ where: { user: user, project: null }, relations: ['user', 'project', 'company', 'workSchedule'] })
        const foundWorkSchedule = await this.workScheduleRepository.findOne({where: {employee: foundEmployee}, relations: ['employee', 'project']})
        foundWorkSchedule.project = project;

        if (!foundEmployee) {
            throw new BadRequestException('no employees are available')
        } else {
            foundEmployee.project = project;
            foundEmployee.company = project.company;
            await this.employeeRepository.save(foundEmployee);
        }        
        const updateProject = await this.projectRepository.findOne(projectID);
        if (foundEmployee.workSchedule === null) {
            await this.workSchedule.createWorkSchedule( [this.transform.toReturnEmployeeDTO(foundEmployee)]);
        } 
        
        await this.workScheduleRepository.save(foundWorkSchedule);
        const checkEmployee = await this.employeeRepository.findOne({where: {id: foundEmployee.id}, relations: ['workSchedule']})
        return this.transform.toReturnProjectDTO(updateProject);
    }

    async deleteEmployee(projectId: number, employeeId: number): Promise<ReturnProjectDTO> {

        const project = await this.projectRepository.findOne(projectId, { relations: ['employees', 'employees.company', 'employees.user', 'company'] });
        if (!project) {
            throw new BadRequestException('Such project doesn\'t exist')
        }

        const foundEmployee = await this.employeeRepository.findOne({ where: { id: employeeId }, relations: ['project'] });
        const foundCompany = await this.companyRepository.findOne({ where: { country: project.company.country }, relations: ['employees'] })
        const foundWorkSchedule = await this.workScheduleRepository.findOne({ where: { employee: foundEmployee }, relations: ['employee', 'project'] })

        if (!foundEmployee) {
            throw new BadRequestException('Employee does not exist!');
        }

        const newEmployees = project.employees.filter(item => item !== foundEmployee);
        const newEmployeesCompany = foundCompany.employees.filter(employee => employee !== foundEmployee);
        foundCompany.employees.splice(0, foundCompany.employees.length, ...newEmployeesCompany)
        project.employees.splice(0, project.employees.length, ...newEmployees);
        await this.projectRepository.save(project);
        await this.companyRepository.save(foundCompany);
        foundEmployee.project = null;
        foundEmployee.desk = null;
        foundWorkSchedule.project = null;
        await this.employeeRepository.save(foundEmployee);
        await this.workScheduleRepository.save(foundWorkSchedule);

        return this.transform.toReturnProjectDTO(project);
    }

}
