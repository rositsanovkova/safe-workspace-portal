import { Controller, Post, Body, Put, UseGuards, Param, Get, Delete } from '@nestjs/common';
import { CreateProjectDTO } from 'src/dtos/project/create-project.dto';
import { ReturnProjectDTO } from 'src/dtos/project/return-project.dto';
import { ProjectService } from 'src/services/project.service';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/enums/user-role';
import { UpdateCompanyDTO } from 'src/dtos/company/update-company.dto';
import { AssignEmployeeDTO } from 'src/dtos/employee/assign-employee.dto';

@Controller('project')
export class ProjectController {
  public constructor(private readonly projectService: ProjectService) { }

  @Get()
  @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async getAll(): Promise<ReturnProjectDTO[]> {

    return this.projectService.getAll();
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async getById(@Param('id') project: string): Promise<ReturnProjectDTO> {

    return this.projectService.getById(+project);
  }

  @Post('company/:id')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async addNewProject(@Body() project: CreateProjectDTO, @Param('id') companyId: string): Promise<ReturnProjectDTO> {

    return this.projectService.createProject(project, +companyId);
  }

  @Put(':id/country')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async updateCountry(@Body() country: UpdateCompanyDTO, @Param('id') projectId: string): Promise<ReturnProjectDTO> {

    return this.projectService.updateCountry(country, +projectId);
  }

  @Put(':id/employees')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async assignEmployees(@Body() employee: AssignEmployeeDTO, @Param('id') projectId: string): Promise<ReturnProjectDTO> {

    return this.projectService.assignEmployee(employee, +projectId);
  }

  @Delete(':projectid/delete/:employeeid')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async deleteEmployee(@Param('projectid') projectId: string, @Param('employeeid') employeeId: string): Promise<ReturnProjectDTO> {

    return this.projectService.deleteEmployee(+projectId, +employeeId);
  }
}
