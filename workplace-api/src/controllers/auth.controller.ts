import { Controller, Post, Body, ValidationPipe, Delete } from '@nestjs/common';
import { AuthService } from 'src/services/auth.service';
import { UserLoginUsernameDTO } from 'src/dtos/users/user-login-username.dto';
import { UserLoginMailDTO } from 'src/dtos/users/user-login-mail.dto';
import { GetToken } from 'src/auth/get-token.decorator';

@Controller('session')
export class AuthController {

    constructor(
        private readonly authService: AuthService,
    ) { }

    @Post('mail')
    async loginWithMail(@Body(new ValidationPipe({ whitelist: true })) userDto: UserLoginMailDTO): Promise<{ token: string }> {
        
        return await this.authService.loginWithMail(userDto.mail, userDto.password);
    }

    @Post('username')
    async loginWithUsername(@Body(new ValidationPipe({ whitelist: true })) userDto: UserLoginUsernameDTO): Promise<{ token: string }> {
        
        return await this.authService.loginWithUsername(userDto.username, userDto.password);
    }

    @Delete()
    async logout(@GetToken() token: string): Promise<{ message: string }> {
        await this.authService.blacklist(token?.slice(7));

        return {
            message: 'You have been logged out!',
        };
    }

}
