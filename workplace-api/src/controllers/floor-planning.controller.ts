import { Controller, Get, UseGuards, Param, Post, Body } from '@nestjs/common';
import { ReturnFloorPlanningDTO } from 'src/dtos/floor-planning/return-floor-planning.dto';
import { FloorPlanningService } from 'src/services/floor-planning.service';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/enums/user-role';
import { AuthGuard } from '@nestjs/passport';
import { CreateFloorPlanningDTO } from 'src/dtos/floor-planning/create-floor-planning.dto';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { ReturnCompanyDTO } from 'src/dtos/company/return-company.dto';

@Controller('floor-plannings')
export class FloorPlanningController {
    public constructor(
        private readonly floorPlanningService: FloorPlanningService

    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async allFloorPlannings(): Promise<ReturnFloorPlanningDTO[]> {

        return await this.floorPlanningService.getAll();
    }

    @Get('/:id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async getCompanyFloorPlanning(@Param('id') floorPlanningId: string): Promise<ReturnFloorPlanningDTO> {

        return await this.floorPlanningService.getById(+floorPlanningId);
    }


    @Post()
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async createFloorPlanning(@Body() floorPlanningDto: CreateFloorPlanningDTO): Promise<ReturnMessageDTO> {

        return await this.floorPlanningService.create(floorPlanningDto);
    }

    @Get('companies/available')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async getCompaniesWithoutFloorPlanning(): Promise<ReturnCompanyDTO[]> {

        return await this.floorPlanningService.getAllCompaniesWithoutFloorPlanning();
    }
}
