import { Controller, Get, UseGuards, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { CompanyService } from 'src/services/company.service';
import { ReturnCompanyDTO } from 'src/dtos/company/return-company.dto';


@Controller('companies')
export class CompanyController {
    public constructor(private readonly companyService: CompanyService) { }

    @Get()
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async allCompanies(): Promise<ReturnCompanyDTO[]> {

        return await this.companyService.getAll();
    }

    @Get(':id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async getById(@Param('id') companyId: string): Promise<ReturnCompanyDTO> {

        return await this.companyService.getByID(+companyId);
    }

}
