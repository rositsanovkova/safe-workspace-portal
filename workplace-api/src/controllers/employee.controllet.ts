import { Controller, Get, UseGuards, Param } from '@nestjs/common';
import { EmployeeService } from 'src/services/employee.service';
import { AuthGuard } from '@nestjs/passport';
import { ReturnEmployeeDTO } from 'src/dtos/employee/return-employee.dto';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/enums/user-role';

@Controller('employees')
export class EmployeeController {
  public constructor(private readonly employeeService: EmployeeService) { }

  @Get()
  @UseGuards(AuthGuard('jwt'), BlacklistGuard)
  async allEmployees(): Promise<ReturnEmployeeDTO[]> {

    return await this.employeeService.getAll();
  }

  @Get('available/:id')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
  async allAvailableEmployees(@Param('id') countryId: string): Promise<ReturnEmployeeDTO[]> {

    return await this.employeeService.getAllAvailable(+countryId);
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard)
  async getById(@Param('id') employeeId: string): Promise<ReturnEmployeeDTO> {

    return await this.employeeService.getById(+employeeId);
  }
}
