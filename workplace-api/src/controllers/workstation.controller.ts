import { Controller, Get, UseGuards, Param, Post, Body } from '@nestjs/common';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/enums/user-role';
import { AuthGuard } from '@nestjs/passport';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { WorkStationService } from 'src/services/workstation.service';
import { ReturnWorkstationDTO } from 'src/dtos/workstation/return-workstation.dto';
import { CreateWorkstationDTO } from 'src/dtos/workstation/create-workstation.dto';


@Controller('workstations')
export class WorkStationsController {
    public constructor(private readonly workStationsService: WorkStationService) { }

    @Get()
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async allWorkStations(): Promise<ReturnWorkstationDTO[]> {

        return await this.workStationsService.getAll();
    }

    @Get('/:id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async getWorkStationById(@Param('id') floorPlanningId: string): Promise<ReturnWorkstationDTO> {

        return await this.workStationsService.getById(+floorPlanningId);
    }

    @Post()
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async createWorkStation(@Body() workStationDto: CreateWorkstationDTO): Promise<ReturnMessageDTO> {

        return await this.workStationsService.create(workStationDto);
    }
}
