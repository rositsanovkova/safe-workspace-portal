import { Body, Controller, Get, Put } from '@nestjs/common';
import { UpdateCountryDTO } from 'src/dtos/country/update-coutry.dto';
import { Countries } from 'src/models/countries.entity';
import { CountryService } from 'src/services/country.service';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { UseGuards } from '@nestjs/common/decorators/core/use-guards.decorator';
import { UserRole } from 'src/enums/user-role';
import { RolesGuard } from 'src/auth/roles.guard';


@Controller('countries')
export class CountryController {
    public constructor(private readonly countryService: CountryService) { }

    @Get()
    async allCountries(): Promise<Countries[]> {

        return await this.countryService.getAll();
    }

    @Put()
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async updateCountryRatio(@Body() updateCountryDTO: UpdateCountryDTO): Promise<void> {

        return await this.countryService.update(updateCountryDTO)
    }
}
