import { Controller, Get, UseGuards, Param, Post, Body, Put } from '@nestjs/common';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/enums/user-role';
import { AuthGuard } from '@nestjs/passport';
import { VacationService } from 'src/services/vacation.service';
import { UserId } from 'src/auth/user-id.decorator';
import { ReturnVacationDTO } from 'src/dtos/vacation/return-vacation.dto';
import { CreateVacationDTO } from 'src/dtos/vacation/create-vacation.dto';
import { UpdateVacationDTO } from 'src/dtos/vacation/update-vacation.dto';


@Controller('vacations')
export class VacationController {
    public constructor(private readonly vacationService: VacationService) { }

    @Get()
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async allVacations(): Promise<ReturnVacationDTO[]> {

        return await this.vacationService.getAll();
    }

    @Get('/:id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async geVacationById(@Param('id') vacationId: string): Promise<ReturnVacationDTO> {

        return await this.vacationService.getById(+vacationId);
    }


    @Post()
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async createVacation(@Body() vacationDto: CreateVacationDTO, @UserId() loggedUser: number): Promise<ReturnVacationDTO> {
        
        return await this.vacationService.create(vacationDto, loggedUser);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'), BlacklistGuard)
    async update(@Body() vacationDto: UpdateVacationDTO, @Param('id') vacationId: string): Promise<ReturnVacationDTO> {
        
        return await this.vacationService.update(vacationDto, +vacationId);
    }

}
