import { Controller, Get, UseGuards, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { CurrentEmployeeService } from 'src/services/current-employee.service';
import { ReturnWorkingScheduleDTO } from 'src/dtos/currentEmployee/return-working-schedule.dto';

@Controller('current-employees')
export class CurrentEmployeeController {
  public constructor(private readonly currentEmployeeService: CurrentEmployeeService) { }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard)
  async getById(@Param('id') companyId: string): Promise<ReturnWorkingScheduleDTO[]> {

    return await this.currentEmployeeService.getByCompany(+companyId);
  }
}
