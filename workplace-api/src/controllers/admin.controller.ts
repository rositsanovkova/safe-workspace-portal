import { AdminService } from 'src/services/admin.service';
import { RolesGuard } from 'src/auth/roles.guard';
import { UpdateUserRoleDTO } from 'src/dtos/users/update-user-role.dto';
import { UserRole } from 'src/enums/user-role';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { AuthGuard } from '@nestjs/passport';
import { Controller,  Body, Put, UseGuards, Param } from '@nestjs/common';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';


@Controller('admin')
export class AdminController {
  public constructor(private readonly adminService: AdminService) { }

  @Put('users/:userId/changeRole')
  @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
  async changeRole(@Body() user: UpdateUserRoleDTO, @Param('userId') userId: string): Promise<ReturnUserDTO> {

      return await this.adminService.changeRole(user, +userId);
  }

}
