import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/services.module';
import { AuthController } from './auth.controller';
import { UsersController } from './user.controller';
import { ProjectController } from './project.controller';
import { EmployeeController } from './employee.controllet';
import { CompanyController } from './company.controller';
import { CountryController } from './country.controller';
import { FloorPlanningController } from './floor-planning.controller';
import { WorkStationsController } from './workstation.controller';
import { CurrentEmployeeController } from './current-employee.controller';
import { VacationController } from './vacation.controller';
import { WorkScheduleController } from './work-schedule.controller';
import { AdminController } from './admin.controller';

@Module({
    imports: [ServicesModule],
    controllers: [AuthController, AdminController, UsersController, ProjectController, EmployeeController, CompanyController, CountryController, FloorPlanningController, WorkStationsController, CurrentEmployeeController, VacationController, WorkScheduleController]
})
export class ControllersModule { }
