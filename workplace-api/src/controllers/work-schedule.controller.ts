import { Controller, Get, UseGuards } from '@nestjs/common';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/enums/user-role';
import { AuthGuard } from '@nestjs/passport';
import { WorkScheduleService } from 'src/services/work-schedule.service';
import { ReturnWorkingScheduleDTO } from 'src/dtos/currentEmployee/return-working-schedule.dto';

@Controller('work-schedules')
export class WorkScheduleController {
    public constructor(private readonly workScheduleService: WorkScheduleService) { }

    @Get()
    @UseGuards(AuthGuard('jwt'), BlacklistGuard, new RolesGuard(UserRole.Admin))
    async allWorkSchedules(): Promise<ReturnWorkingScheduleDTO[]> {

        return await this.workScheduleService.getAll();
    }
}
