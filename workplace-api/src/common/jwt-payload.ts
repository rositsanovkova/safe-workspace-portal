import { UserRole } from "src/enums/user-role";


export class JWTPayload {
    id: number;
    username: string;
    mail: string;
    role: UserRole;

}
