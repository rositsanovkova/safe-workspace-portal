import { createConnection, Repository, Connection } from 'typeorm';
import fetch from 'node-fetch'
import { Countries } from '../models/countries.entity';
import { DbSetup } from '../models/dbsetup.entity';
import * as bcrypt from 'bcrypt';
import { $enum } from 'ts-enum-util';
import { User } from '../models/user.entity';
import { UserRole } from '../enums/user-role';
import { Company } from '../models/company.entity';
import { Employee } from '../models/employee.entity';

// SENSITIVE DATA ALERT! - Normally the seed and the admin credentials should not be present in the public repository!
// Run: `npm run seed` to seed the database

const seedCountries = async (connection: Connection) => {
  const countriesRepo: Repository<Countries> = connection.manager.getRepository(Countries);
  let count = 0;
  const fetchedCountries = [];

  await fetch('https://restcountries.eu/rest/v2/all')
    .then(function (res) {
      return res.json();
    })
    .then(async (result) => {
      const data = result;
      for (let i = 0; i < data.length; i++) {
        const name = data[i].name;
        const country = {
          id: count++,
          name: name,
        }
        fetchedCountries.push(country);
      }
    })
    .catch();



  for (const country of fetchedCountries) {
    const foundCountry = await countriesRepo.findOne({
      where: {
        name: country.name,
      }
    });
    if (!foundCountry) {
      const bookToCreate = countriesRepo.create(country);
      console.log(await countriesRepo.save(bookToCreate));
    }
  }
}

const seedUsers = async (connection: Connection) => {
  const usersRepo: Repository<User> = connection.manager.getRepository(User);
  const roleNames = $enum(UserRole).getValues();
  const Basic = roleNames[0];
  const Admin = roleNames[1];
  // console.log(usersRepo);
  const users = [{
    username: 'system_admin',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Ivan Ivanov',
    role: Admin,
    isDeleted: false,
    mail: 'ivan_ivan@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'georgi_georgiev',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Georgi Georgiev',
    role: Basic,
    isDeleted: false,
    mail: 'georgi_georgiev@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'petar_petrov',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Petar Petrov',
    role: Basic,
    isDeleted: false,
    mail: 'petar_petrov@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'dimitar_dimitrov',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Dimitar Dimitrov',
    role: Basic,
    isDeleted: false,
    mail: 'dimitar_dimitrov@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'nikolai_nikolov',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Nikolai Nikolov',
    role: Basic,
    isDeleted: false,
    mail: 'nikolai_nikolov@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'milen_dimitrov',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Milen Dimitrov',
    role: Basic,
    isDeleted: false,
    mail: 'milen_dimitrov@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'rositsa_novkova',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Rositsa Novkova',
    role: Basic,
    isDeleted: false,
    mail: 'rositsa_novkova@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'silviya_naumova',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Silviya Naumova',
    role: Basic,
    isDeleted: false,
    mail: 'silviya_naumova@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'viktoriya_vankova',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Viktoriya Vankova',
    role: Basic,
    isDeleted: false,
    mail: 'viktoriya_vankova@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'anastasiya_dicheva',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Anastasiya Dicheva',
    role: Basic,
    isDeleted: false,
    mail: 'anastasiya_dicheva@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'lora_lilova',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Lora Lilova',
    role: Basic,
    isDeleted: false,
    mail: 'lora_lilova@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'polia_tomova',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Polia Tomova',
    role: Basic,
    isDeleted: false,
    mail: 'polia_tomova@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'maria_spasova',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Maria Spasova',
    role: Basic,
    isDeleted: false,
    mail: 'maria_spasova@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'antoniya_kostadinova',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Antoniya Kostadinova',
    role: Basic,
    isDeleted: false,
    mail: 'antoniya_kostadinova@swp.com',
    country: 'Bulgaria'
  },
  {
    username: 'caroline_ciel',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Caroline Ciel',
    role: Basic,
    isDeleted: false,
    mail: 'caroline_ciel@swp.com',
    country: 'France'
  },
  {
    username: 'elise_ferrand',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Elise Ferrand',
    role: Basic,
    isDeleted: false,
    mail: 'elise_ferrand@swp.com',
    country: 'France'
  },
  {
    username: 'avril_bruse',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Avril Bruse',
    role: Basic,
    isDeleted: false,
    mail: 'avril_bruse@swp.com',
    country: 'France'
  },
  {
    username: 'monique_oliver',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Monique Oliver',
    role: Basic,
    isDeleted: false,
    mail: 'monique_oliver@swp.com',
    country: 'France'
  },
  {
    username: 'sharice_cide',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Sharice Cide',
    role: Basic,
    isDeleted: false,
    mail: 'sharice_cide@swp.com',
    country: 'France'
  },
  {
    username: 'sennett_satch',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Sennett Satch',
    role: Basic,
    isDeleted: false,
    mail: 'sennett_satch@swp.com',
    country: 'France'
  },
  {
    username: 'varil_umber',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Varil Umber',
    role: Basic,
    isDeleted: false,
    mail: 'varil_umber@swp.com',
    country: 'France'
  },
  {
    username: 'yannick_vere',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Yannick Vere',
    role: Basic,
    isDeleted: false,
    mail: 'yannick_vere@swp.com',
    country: 'France'
  },
  {
    username: 'nico_mavridis',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Nico Mavridis',
    role: Basic,
    isDeleted: false,
    mail: 'nico_mavridis@swp.com',
    country: 'Greece'
  },
  {
    username: 'demeter_demeter',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Demeter Demeter',
    role: Basic,
    isDeleted: false,
    mail: 'demeter_demeter@swp.com',
    country: 'Greece'
  },
  {
    username: 'magdalene_leo',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Magdalene Leo',
    role: Basic,
    isDeleted: false,
    mail: 'magdalene_leo@swp.com',
    country: 'Greece'
  },
  {
    username: 'eleni_christos',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Eleni Christos',
    role: Basic,
    isDeleted: false,
    mail: 'eleni_christos@swp.com',
    country: 'Greece'
  },
  {
    username: 'adelina_marin',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Adelina Marin',
    role: Basic,
    isDeleted: false,
    mail: 'adelina_marin@swp.com',
    country: 'Romania'
  },
  {
    username: 'claudia_stefan',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Claudia Stefan',
    role: Basic,
    isDeleted: false,
    mail: 'claudia_stefan@swp.com',
    country: 'Romania'
  },
  {
    username: 'toma_toma',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Toma Toma',
    role: Basic,
    isDeleted: false,
    mail: 'toma_toma@swp.com',
    country: 'Romania'
  },
  {
    username: 'sebastian_simov',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Sebastian Simov',
    role: Basic,
    isDeleted: false,
    mail: 'sebastian_simov@swp.com',
    country: 'Romania'
  },
  {
    username: 'maximilian_gunzel',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Maximilian Gunzel',
    role: Basic,
    isDeleted: false,
    mail: 'maximilian_gunzel@swp.com',
    country: 'Germany'
  },
  {
    username: 'klaus_fred',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Klaus Fred',
    role: Basic,
    isDeleted: false,
    mail: 'klaus_fred@swp.com',
    country: 'Germany'
  },
  {
    username: 'mona_jedke',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Mona Jedke',
    role: Basic,
    isDeleted: false,
    mail: 'mona_jedke@swp.com',
    country: 'Germany'
  },
  {
    username: 'emma_ninas',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Emma Ninas',
    role: Basic,
    isDeleted: false,
    mail: 'emma_ninas@swp.com',
    country: 'Germany'
  },
  {
    username: 'anna_luk',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Anna Luk',
    role: Basic,
    isDeleted: false,
    mail: 'anna_luk@swp.com',
    country: 'Netherlands'
  },
  {
    username: 'sofia_dex',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Sofia Dex',
    role: Basic,
    isDeleted: false,
    mail: 'sofia_dex@swp.com',
    country: 'Netherlands'
  },
  {
    username: 'boris_xavi',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Boris Xavi',
    role: Basic,
    isDeleted: false,
    mail: 'boris_xavi@swp.com',
    country: 'Netherlands'
  },
  {
    username: 'dani_kay',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Dani Kay',
    role: Basic,
    isDeleted: false,
    mail: 'dani_kay@swp.com',
    country: 'Netherlands'
  },
  {
    username: 'julia_nathan',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Julia Nathan',
    role: Basic,
    isDeleted: false,
    mail: 'julia_nathan@swp.com',
    country: 'Belgium'
  },
  {
    username: 'vick_maths',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Vick Maths',
    role: Basic,
    isDeleted: false,
    mail: 'vick_maths@swp.com',
    country: 'Belgium'
  },
  {
    username: 'jasmin_noah',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Jasmin Noah',
    role: Basic,
    isDeleted: false,
    mail: 'jasmin_noah@swp.com',
    country: 'Austria'
  },
  {
    username: 'felix_louis',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Felix Louis',
    role: Basic,
    isDeleted: false,
    mail: 'felix_louis@swp.com',
    country: 'Austria'
  },
  {
    username: 'bogumil_leszek',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Bogumil Leszek',
    role: Basic,
    isDeleted: false,
    mail: 'bogumil_leszek@swp.com',
    country: 'Poland'
  },
  {
    username: 'iana_stanislav',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Iana Stanislav',
    role: Basic,
    isDeleted: false,
    mail: 'iana_stanislav@swp.com',
    country: 'Poland'
  },
  {
    username: 'milica_grlovic',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Milica Grlovic',
    role: Basic,
    isDeleted: false,
    mail: 'milica_grlovic@swp.com',
    country: 'Serbia'
  },
  {
    username: 'velimir_mihajlovic',
    password: await bcrypt.hash('admin1', 10),
    fullName: 'Velimir Mihajlovic',
    role: Basic,
    isDeleted: false,
    mail: 'velimir_mihajlovic@swp.com',
    country: 'Serbia'
  },
  ];

  for (const user of users) {
    const foundUser = await usersRepo.findOne({
      username: user.username,
    });

    if (!foundUser) {
      const userToCreate = usersRepo.create(user);
      console.log(await usersRepo.save(userToCreate));
    }
  }

};

const seedCompanies = async (connection: Connection) => {
  const companiesRepo: Repository<Company> = connection.manager.getRepository(Company);

  // console.log(usersRepo);
  const companies = [{
    country: 'Bulgaria'
  },
  {
    country: 'Greece'
  },
  {
    country: 'Romania'
  },
  {
    country: 'Germany'
  }, 
  {
    country: 'France'
  },  
  {
    country: 'Netherlands'
  },
  {
    country: 'Belgium'
  },
  {
    country: 'Austria'
  },
  {
    country: 'Poland'
  },
  {
    country: 'Serbia'
  },
  ];

  for (const company of companies) {
    const foundUser = await companiesRepo.findOne({
      country: company.country,
    });

    if (!foundUser) {
      const userToCreate = companiesRepo.create(company);
      console.log(await companiesRepo.save(userToCreate));
    }
  }

};

const seedEmployees = async (connection: Connection) => {
  const employeeRepo: Repository<Employee> = connection.manager.getRepository(Employee);
  const usersRepo: Repository<User> = connection.manager.getRepository(User);
  const companyRepo: Repository<Company> = connection.manager.getRepository(Company);
  const foundUsers = await usersRepo.find();
  const mappedUsers = async () => {
    const users = await foundUsers.map(async user => {
      const employee = employeeRepo.create();
      employee.user = user;
      employee.isDeleted = false;
      employee.company = await companyRepo.findOne({where: {country: user.country}})
      console.log(await employeeRepo.save(employee));
      // console.log(await employeeRepo.save(employee));
    })
    return Promise.all(users)
  }
  const users = await mappedUsers();
  console.log(users)
};

const beginSetup = async (connection: Connection) => {
  const dbsetupRepo = connection.manager.getRepository(DbSetup);
  const setupData = await dbsetupRepo.find({});

  if (setupData.length) {
    throw new Error(`Data has been already set up`);
  }
};

const completeSetup = async (connection: Connection) => {
  const dbsetupRepo = connection.manager.getRepository(DbSetup);

  await dbsetupRepo.save({ message: 'Setup has been completed!' });
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  try {
    await beginSetup(connection);

    await seedUsers(connection);
    await seedCountries(connection);
    await seedCompanies(connection);
    await seedEmployees(connection);

    await completeSetup(connection);
  } catch (e) {
    console.log(e.message)
  }

  console.log('Seed completed!');
  connection.close();
};

seed().catch(console.error);
