import { PrimaryGeneratedColumn, Column, Entity, ManyToOne, OneToMany } from "typeorm";
import { Company } from "./company.entity";
import { Employee } from "./employee.entity";

@Entity()
export class Project {
    @PrimaryGeneratedColumn('increment')
    id: number;
    @ManyToOne(() => Company, company => company.projects)
    company: Company;
    @Column({ type: 'nvarchar', nullable: false, length: 200, default: '' })
    title: string;
    @OneToMany(() => Employee, employees => employees.project)
    employees: Employee[];
}
