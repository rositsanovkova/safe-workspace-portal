import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn } from "typeorm";
import { WorkStation } from "./workstation.entity";
import { Company } from "./company.entity";

@Entity()
export class FloorPlanning {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ type: 'int', default: 0 })
    numberOfDesks: number;
    @Column({ type: 'int', default: 0 })
    workStationColumns: number;
    @OneToMany( () => WorkStation, workStation => workStation.floorPlanning)
    @JoinColumn()
    workStation: WorkStation[];
    @OneToOne( () => Company, company => company.floorPlanning)
    @JoinColumn()
    company: Company;
}
