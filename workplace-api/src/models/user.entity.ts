import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany } from 'typeorm';
import { UserRole } from '../enums/user-role';
import { Employee } from './employee.entity';
import { Vacation } from './vacation.entity';

@Entity()
export class User {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ type: 'nvarchar', nullable: false, unique: true, length: 20, default: '' })
    username: string;

    @Column({ type: 'nvarchar', nullable: false, default: '' })
    fullName: string;

    @Column({ type: 'nvarchar', nullable: false, default: '' })
    password: string;
    @Column({ unique: true })
    mail: string;
    @Column()
    country: string;
    @Column({ type: 'enum', enum: UserRole, default: UserRole.Basic })
    role: UserRole;
    @Column({ type: 'boolean', default: false })
    isDeleted: boolean;

    @OneToOne(() => Employee, employee => employee.user)
    @JoinColumn()
    employee: Employee;
    @OneToMany(() => Vacation, vacation => vacation.user)

    vacation: Vacation[];
}
