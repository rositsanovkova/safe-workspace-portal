import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "./user.entity";
import { VacationStatus } from "../enums/vacation-status";

@Entity()
export class Vacation {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ type: 'nvarchar' })
    startDate: string;
    @Column({ type: 'nvarchar' })
    endDate: string;
    @ManyToOne(() => User, user => user.id)
    user: User;
    @Column({ type: 'enum', enum: VacationStatus, default: VacationStatus.Requested })
    status: VacationStatus;
}
