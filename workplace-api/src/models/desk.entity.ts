import { Entity, PrimaryGeneratedColumn, Column, OneToOne, ManyToOne, JoinColumn } from "typeorm";
import { Employee } from "./employee.entity";
import { DeskStatus } from "../enums/desk-status";
import { WorkStation } from "./workstation.entity";

@Entity()
export class Desk {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(() => Employee, employee => employee.desk)
    @JoinColumn()
    employee: Employee;

    @Column({ default: DeskStatus.Available })
    status: DeskStatus;

    @OneToOne(() => Employee, employee => employee.desk)
    @JoinColumn()
    employeeNextWeek: Employee;

    @Column({ type: 'enum', enum: DeskStatus, default: DeskStatus.Available })
    statusNextWeek: DeskStatus;

    @ManyToOne(() => WorkStation, workstation => workstation.desks)
    @JoinColumn()
    workstation: WorkStation;
}
