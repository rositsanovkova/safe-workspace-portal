import { PrimaryGeneratedColumn, Column, OneToMany, Entity, OneToOne } from "typeorm";
import { Project } from "./projects.entity";
import { FloorPlanning } from "./floor-planning.entity";
import { Employee } from "./employee.entity";

@Entity()
export class Company {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column()
    country: string;


    @OneToMany(() => Project, projects => projects.company)
    projects: Project[];

    @OneToOne(() => FloorPlanning, floorPlanning => floorPlanning.company)
    floorPlanning: FloorPlanning;

    @OneToMany(() => Employee, employees => employees.company)
    employees: Employee[];

    @Column('decimal', { precision: 5, scale: 2, default: 0 })
    ratio: number;
}
