import { PrimaryGeneratedColumn, Column, Entity } from "typeorm";

@Entity()
export class Countries {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column()
    name: string;


    @Column('decimal', { precision: 5, scale: 2, default: 0.05 })
    minRatio: number;


    @Column('decimal', { precision: 5, scale: 2, default: 0.10 })
    maxRatio: number;
}
