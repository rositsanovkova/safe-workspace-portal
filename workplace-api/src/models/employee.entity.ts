import { PrimaryGeneratedColumn, Column, OneToOne, Entity, ManyToOne, JoinColumn, JoinTable } from "typeorm";
import { User } from "./user.entity";
import { Project } from "./projects.entity";
import { Company } from "./company.entity";
import { Desk } from "./desk.entity";
import { WorkSchedule } from "./work-schedule.entity";

@Entity()
export class Employee {
    @PrimaryGeneratedColumn('increment')
    id: number;
    @OneToOne(() => User, user => user.employee)
    @JoinColumn()
    user: User;
    @OneToOne(() => WorkSchedule, workSchedule => workSchedule.employee)
    @JoinColumn()
    workSchedule: WorkSchedule;
    @ManyToOne(() => Project, projects => projects.employees)
    @JoinTable()
    project: Project;
    @ManyToOne(() => Company, company => company.employees)
    company: Company;
    @OneToOne(() => Desk, desk => desk.employee)
    @JoinColumn()
    desk: Desk;
    @Column({ type: 'boolean', default: false })
    isDeleted: boolean;
}
