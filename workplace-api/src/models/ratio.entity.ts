import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn } from "typeorm";

@Entity()
export class Ratio {
    @PrimaryGeneratedColumn('increment')
    id: number;
    @Column({ type: 'nvarchar', nullable: false, length: 50, default: '' })
    country: string;
    @Column({ type: 'int', default: 0 })
    dailyCases: number;
    @Column({ type: 'int', default: 0 })
    millTests: number;
    @CreateDateColumn()
    date: Date;
}
