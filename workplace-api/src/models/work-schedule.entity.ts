import { PrimaryGeneratedColumn, Column, OneToOne, Entity, ManyToOne, JoinColumn } from "typeorm";
import { Project } from "./projects.entity";
import { Company } from "./company.entity";
import { EmployeeLocation } from "../enums/employee-location";
import { Employee } from "./employee.entity";

@Entity()
export class WorkSchedule {
    @PrimaryGeneratedColumn('increment')
    id: number;
    @OneToOne( () => Employee, employee => employee.workSchedule)
    @JoinColumn()
    employee: Employee;
    @ManyToOne( () => Project, projects => projects.id)
    project: Project; 
    @ManyToOne( () => Company, company => company.id)
    company: Company;
    @Column({ type: 'enum', enum: EmployeeLocation, default: EmployeeLocation.Home})
    currentWeekLocation: EmployeeLocation;
    @Column({ type: 'enum', enum: EmployeeLocation, default: EmployeeLocation.Home})
    nextWeekLocation: EmployeeLocation;
}
