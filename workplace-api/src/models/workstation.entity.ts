import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, JoinColumn } from "typeorm";
import { Desk } from "./desk.entity";
import { FloorPlanning } from "./floor-planning.entity";

@Entity()
export class WorkStation {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    rowDesks: number;
    @Column()
    columnDesks: number;    
    @OneToMany( () => Desk, desk => desk.workstation)
    @JoinColumn()
    desks: Desk[];
    @ManyToOne( () => FloorPlanning, floorPlanning => floorPlanning.workStation)
    @JoinColumn()
    floorPlanning: FloorPlanning;
}