import { DeskStatus } from "src/enums/desk-status";
import { ReturnWorkstationDTO } from "../workstation/return-workstation.dto";
import { ReturnEmployeeDTO } from "../employee/return-employee.dto";
import { IsEnum, IsInstance, IsInt } from "class-validator";

export class ReturnDeskDTO {
    @IsInt()
    id: number;
    @IsInstance(ReturnEmployeeDTO)
    employee: ReturnEmployeeDTO;
    @IsEnum(DeskStatus)
    status: DeskStatus;
    @IsInstance(ReturnEmployeeDTO)
    employeeNextWeek: ReturnEmployeeDTO;
    @IsEnum(DeskStatus)
    statusNextWeek: DeskStatus;
    @IsInstance(ReturnWorkstationDTO)
    workstation: ReturnWorkstationDTO;
}
