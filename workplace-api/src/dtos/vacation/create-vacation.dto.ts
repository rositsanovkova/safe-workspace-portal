import { IsNotEmpty } from "class-validator";

export class CreateVacationDTO {
    @IsNotEmpty()
    startDate: string;
    @IsNotEmpty()
    endDate: string;
}
