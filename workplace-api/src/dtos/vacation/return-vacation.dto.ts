import { ReturnUserDTO } from "../users/return-user.dto";
import { VacationStatus } from "src/enums/vacation-status";

export class ReturnVacationDTO {
    id: number;
    startDate: string;
    endDate: string;
    user: ReturnUserDTO;
    status: VacationStatus;
}
