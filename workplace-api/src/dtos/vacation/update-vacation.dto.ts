import { IsEnum } from "class-validator";
import { VacationStatus } from "src/enums/vacation-status";

export class UpdateVacationDTO {
    @IsEnum(VacationStatus)
    status: VacationStatus;
}
