import { IsString, MaxLength, MinLength } from "class-validator";

export class AssignEmployeeDTO {
    @IsString()
    @MinLength(5)
    @MaxLength(100)
    fullName: string;
}
