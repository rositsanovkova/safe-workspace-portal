import { ReturnUserDTO } from "../users/return-user.dto";
import { ReturnProjectDTO } from "../project/return-project.dto";
import { ReturnCompanyDTO } from "../company/return-company.dto";
import { ReturnDeskDTO } from "../desk/return-desk.dto";
import { ReturnWorkingScheduleDTO } from "../currentEmployee/return-working-schedule.dto";

export class ReturnEmployeeDTO {
    id: number;
    user: ReturnUserDTO;
    project: ReturnProjectDTO;
    company: ReturnCompanyDTO;
    desk: ReturnDeskDTO;
    workSchedule: ReturnWorkingScheduleDTO;
}
