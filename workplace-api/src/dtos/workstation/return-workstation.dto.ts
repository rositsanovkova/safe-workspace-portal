import { ReturnFloorPlanningDTO } from "../floor-planning/return-floor-planning.dto";
import { ReturnDeskDTO } from "../desk/return-desk.dto";

export class ReturnWorkstationDTO {
    id: number;
    rowDesks: number;
    columnDesks: number;
    desks: ReturnDeskDTO[];
    floorPlanning: ReturnFloorPlanningDTO;
}
