export class CreateWorkstationDTO {
    rowDesks: number;
    columnDesks: number;
    floorPlanning: number;
}
