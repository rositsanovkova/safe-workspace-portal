import { IsEnum } from "class-validator";
import { UserRole } from "src/enums/user-role";

export class UpdateUserRoleDTO{
    @IsEnum(UserRole)
    role: UserRole;
}
