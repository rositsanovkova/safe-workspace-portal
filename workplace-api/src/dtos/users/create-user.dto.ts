import { Length, IsString, IsEmail, MaxLength, MinLength } from 'class-validator';

export class CreateUserDTO {
  @IsString()
  @Length(4, 20)
  username: string;
  @IsString()
  @Length(4, 100)
  fullName: string;

  @IsString()
  @Length(4, 20)
  password: string;
  @IsEmail()
  mail: string;
  @IsString()
  @MinLength(2)
  @MaxLength(50)
  country: string;
}
