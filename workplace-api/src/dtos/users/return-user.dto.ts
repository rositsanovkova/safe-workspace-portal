import { UserRole } from "src/enums/user-role";
import { ReturnEmployeeDTO } from "../employee/return-employee.dto";
import { ReturnVacationDTO } from "../vacation/return-vacation.dto";

export class ReturnUserDTO {
  id: number;
  username: string;
  fullName: string;
  country: string;
  mail: string;
  employee: ReturnEmployeeDTO;
  vacation: ReturnVacationDTO[];
  role: UserRole;
}
