import { IsString, Length, IsEmail, MaxLength, MinLength } from 'class-validator';

export class UpdateUserDTO {
    @IsString()
    @Length(4, 100)
    fullName?: string;
    @IsEmail()
    mail?: string;
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    country?: string;
}
