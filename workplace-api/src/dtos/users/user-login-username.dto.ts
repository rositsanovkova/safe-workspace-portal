import { IsString, IsNotEmpty } from 'class-validator';

export class UserLoginUsernameDTO {
    @IsString()
    @IsNotEmpty()
    username: string;

    @IsString()
    @IsNotEmpty()
    password: string;
}
