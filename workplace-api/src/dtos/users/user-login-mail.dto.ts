import { IsString, IsNotEmpty, IsEmail } from 'class-validator';

export class UserLoginMailDTO {
    @IsEmail()
    @IsNotEmpty()
    mail: string;

    @IsString()
    @IsNotEmpty()
    password: string;
}
