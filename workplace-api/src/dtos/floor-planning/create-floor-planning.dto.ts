import { IsString, MaxLength, MinLength } from "class-validator";

export class CreateFloorPlanningDTO {
    numberOfDesks: number;
    workStationColumns: number;
    @MinLength(2)
    @MaxLength(50)
    @IsString()
    company: string;
}
