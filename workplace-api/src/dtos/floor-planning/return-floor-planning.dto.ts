import { ReturnCompanyDTO } from "../company/return-company.dto";
import { ReturnWorkstationDTO } from "../workstation/return-workstation.dto";

export class ReturnFloorPlanningDTO {
    id: number;
    numberOfDesks: number;
    workStationColumns: number;
    workStation: ReturnWorkstationDTO[];
    company: ReturnCompanyDTO;
}
