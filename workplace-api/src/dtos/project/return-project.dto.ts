import { ReturnEmployeeDTO } from "../employee/return-employee.dto";
import { ReturnCompanyDTO } from "../company/return-company.dto";

export class ReturnProjectDTO {
    id: number;
    title: string;
    employees: ReturnEmployeeDTO[];
    company: ReturnCompanyDTO;
}
