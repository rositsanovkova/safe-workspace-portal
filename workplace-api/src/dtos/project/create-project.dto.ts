import { IsArray, IsString, MaxLength, MinLength } from "class-validator";

export class CreateProjectDTO {
    @IsString()
    @MaxLength(200)
    @MinLength(2)
    title: string;
    @IsArray()
    employees: string[];
}
