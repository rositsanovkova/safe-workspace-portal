import { MinLength, MaxLength, IsDecimal, IsNumber, IsPositive, Max, IsString } from "class-validator";

export class UpdateCountryDTO {
    @MinLength(2)
    @MaxLength(50)
    @IsString()
    name: string;

    @IsNumber()
    @IsPositive()
    @IsDecimal()
    @Max(1)
    minRatio?: string;

    @IsNumber()
    @IsPositive()
    @IsDecimal()
    @Max(1)
    maxRatio?: string;
}
