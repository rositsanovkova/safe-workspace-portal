import { ReturnProjectDTO } from "../project/return-project.dto";
import { ReturnFloorPlanningDTO } from "../floor-planning/return-floor-planning.dto";
import { ReturnEmployeeDTO } from "../employee/return-employee.dto";

export class ReturnCompanyDTO {
    id: number;
    country: string;
    projects: ReturnProjectDTO[];
    floorPlanning: ReturnFloorPlanningDTO;
    employees: ReturnEmployeeDTO[];
    ratio: number
}
