import { MinLength, MaxLength, IsString } from "class-validator";

export class UpdateCompanyDTO {
    @MinLength(2)
    @MaxLength(50)
    @IsString()
    country: string;
}
