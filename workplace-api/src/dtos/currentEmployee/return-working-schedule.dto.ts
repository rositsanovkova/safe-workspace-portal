import { ReturnProjectDTO } from "../project/return-project.dto";
import { ReturnCompanyDTO } from "../company/return-company.dto";
import { EmployeeLocation } from "src/enums/employee-location";
import { ReturnEmployeeDTO } from "../employee/return-employee.dto";

export class ReturnWorkingScheduleDTO {
    id: number;
    employee: ReturnEmployeeDTO;
    project: ReturnProjectDTO;
    company: ReturnCompanyDTO;
    currentWeekLocation: EmployeeLocation;
    nextWeekLocation: EmployeeLocation;
}
