import { IsInstance } from "class-validator";
import { ReturnCompanyDTO } from "../company/return-company.dto";
import { ReturnEmployeeDTO } from "../employee/return-employee.dto";
import { ReturnProjectDTO } from "../project/return-project.dto";

export class UpdateWorkScheduleDTO {
    @IsInstance(ReturnCompanyDTO)
    company?: ReturnCompanyDTO;
    @IsInstance(ReturnEmployeeDTO)
    employees?: ReturnEmployeeDTO[];
    @IsInstance(ReturnProjectDTO)
    project?: ReturnProjectDTO;
}
