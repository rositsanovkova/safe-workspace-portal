export enum EmployeeLocation {
    Home = 1,
    Office = 2,
    Vacation = 3,
}
