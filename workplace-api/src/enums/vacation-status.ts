export enum VacationStatus {
    Requested = 1,
    Approved = 2,
    Rejected = 3,
    Canceled = 4,
}
