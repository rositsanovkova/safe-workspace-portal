export enum DeskStatus {
    Available = 1,
    Forbidden = 2,
    Taken = 3,
}
