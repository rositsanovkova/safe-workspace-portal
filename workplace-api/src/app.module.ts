import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ControllersModule } from './controllers/controllers.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ControllersModule,
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'nevlizai007', //set password
      database: 'workspacedb',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    ScheduleModule.forRoot()
  ],
})
export class AppModule { }
